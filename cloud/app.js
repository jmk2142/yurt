var _ = require('cloud/modules/underscore');
var Buffer = require('buffer').Buffer;

var base = require('cloud/modules/baseFunctions.js');
var getUsername = base.getUsername;
var makeConfig = base.makeConfig;


// These two lines are required to initialize Express in Cloud Code.
var express = require('express');
var parseExpressHttpsRedirect = require('parse-express-https-redirect');
var parseExpressCookieSession = require('parse-express-cookie-session');
var app = express();

// Global app configuration section
app.set('views', 'cloud/views'); // Specify the folder to find templates
app.set('view engine', 'ejs'); // Set the template engine

app.use(parseExpressHttpsRedirect());  // Require user to be on HTTPS.
app.use(express.bodyParser()); // Middleware for reading request body
app.use(express.cookieParser('<3c09m0$&p@r$e&fri3dch!ck3n'));
app.use(parseExpressCookieSession({
	cookie: { maxAge: 1 * 24 * 60 * 60 * 1000 },  // 1 day
}));

// Prevent Caching of main page
// app.use(function noCacheForRoot(req, res, next) {
// 	if (req.url === '/') {
// 		res.header("Cache-Control", "no-cache, no-store, must-revalidate");
// 		res.header("Pragma", "no-cache");
// 		res.header("Expires", 0);
// 	}
// 	next();
// });

// Clears Cookies
// app.get('/setCookies', function(req, res) {
// 	res.clearCookie('fPage');
	
// 	var query = req.query;
// 	if (query.fPage) {
// 		res.cookie('fPage', query.fPage, {
// 			path: '/'
// 		});
// 	}
// 	res.redirect('/');
// });

// DEFAULT ROUTE
app.get('/', function(req, res) {

	// For front page AB testing
	var fPageVer;
	
	// Check if there is AB test cookie
	if (req.cookies.fPage) {
		// Set proper AB test page
		fPageVer = req.cookies.fPage;
	} else {
		// Set a random fPage
		fPageVer = _.sample(['J','M']); // J = Jin, M = Mee versions
		res.cookie('fPage', fPageVer, {});
	}

	if (Parse.User.current()) {
		var Configuration = Parse.Object.extend('Configuration');

		var configQuery = new Parse.Query('Configuration');
				configQuery.equalTo('user', Parse.User.current());

		var promiseUser = Parse.User.current().fetch();
		var promiseConfig = configQuery.first();

		Parse.Promise.when(promiseUser, promiseConfig).then(function(user, config) {

			if (!config) {
				
				// Create a config
				makeConfig(Parse.User.current()).then(function(config) {
					if (user.get('status') === 'active') {

						Parse.Session.current().then(function(session) {
							var sessionToken = session.getSessionToken();
							res.render('research/app.ejs', {
								sessionToken: sessionToken,
								username: user.get('username'),
								configuration: JSON.stringify(config.toJSON()),
								version: _.sample([1,2,3,4,5,6,7,8,9])
							});
						}, function() {
							res.redirect('/logout');
						});

					} else {
						Parse.Session.current().then(function(session) {
							var sessionToken = session.getSessionToken();
							res.render('business/summary.ejs', {
								sessionToken: sessionToken,
								version: _.sample([1,2,3,4,5,6,7,8,9])
							});
						}, function() {
							res.redirect('/logout');
						});
					}
				});
			} else {

				if (user.get('status') === 'active') {

					Parse.Session.current().then(function(session) {
						var sessionToken = session.getSessionToken();
						res.render('research/app.ejs', {
							sessionToken: sessionToken,
							username: user.get('username'),
							configuration: JSON.stringify(config.toJSON()),
							version: _.sample([1,2,3,4,5,6,7,8,9])
						});
					}, function() {
						res.redirect('/logout');
					});

				} else {
					Parse.Session.current().then(function(session) {
						var sessionToken = session.getSessionToken();
						res.render('business/summary.ejs', {
							sessionToken: sessionToken,
							version: _.sample([1,2,3,4,5,6,7,8,9])
						});
					}, function() {
						res.redirect('/logout');
					});

				}

			}
		}, function(error) {
			Parse.User.logOut();
			res.redirect('/');
		});
		
	} else {

		// fPage -> JIN version
		if (fPageVer === 'J') {
			res.render('business/homeTest/homeJ.ejs', {
				version: _.sample([1,2,3,4,5,6,7,8,9])
			});

		// fPage -> MEE version
		} else if (fPageVer === 'M') {
			res.render('business/homeTest/homeM.ejs', {
				version: _.sample([1,2,3,4,5,6,7,8,9])
			});

		// fPage -> DEFAULT version
		} else {
			res.render('business/home.ejs', {
				version: _.sample([1,2,3,4,5,6,7,8,9])
			});
		}

	}
});

// MOBILE DENIAL ROUTE
app.get('/mobile', function(req, res) {
	res.render('business/mobile.ejs', {
		version: _.sample([1,2,3,4,5,6,7,8,9])
	});
});

// CONSENT ROUTE
app.get('/consent', function(req, res) {

	getUsername().then(function(username) {
		res.render('business/consent.ejs', {
			generated: username,
			version: _.sample([1,2,3,4,5,6,7,8,9])
		});
	}, function(error) {
		console.log(error.message);
	});
	
});

// REGISTRATION ROUTE
app.post('/register', function(req, res) {
	var username = req.body.username;
	var password = req.body.password;
	var first = req.body.first;
	var last = req.body.last;
	var email = req.body.email;
	var consent = req.body.consent;

	var userData = {
		username: username,
		first: first,
		last: last,
		email: email
	};

	Parse.Config.get().then(function(config) {
		
		return Parse.User.signUp(username, password, {
			first: first,
			last: last,
			consented: true,
			debriefed: false,
			status:'active',
			state: config.get('state')
		});

	}).then(function(user) {

		var p1 = Parse.Promise.as(user);
		var p2 = makeConfig(user);

		return Parse.Promise.when(p1, p2);

	}).then(function(user, config) {

		var Participation = Parse.Object.extend('Participation');
		var participation = new Participation({
			user: user,
			first: first,
			last: last,
			email: email,
			completion: 'initial'
		});

		var acl = new Parse.ACL();
				acl.setRoleReadAccess('admin', true);
				acl.setRoleWriteAccess('admin', true);

		participation.setACL(acl);

		return participation.save();

	}).then(function(participation) {
		res.redirect('/');
	}, function(error) {
		userData = _.extend(userData, {
			error: error,
			version: _.sample([1,2,3,4,5,6,7,8,9])
		});
		res.render('business/registration.ejs', userData);
	});
});

// LOGIN ROUTE
app.post('/login', function(req, res) {
	var username = req.body.username;
	var password = req.body.password;

	var userData = {
		username: username,
		password: password
	};

	Parse.User.logIn(username, password).then(function(user) {
		res.redirect('/');
	}, function(error) {
		userData = _.extend(userData, {
			error: error,
			version: _.sample([1,2,3,4,5,6,7,8,9])
		});
		res.render('business/login.ejs', userData);
	});
});

// LOGOUT ROUTE
app.get('/logout', function(req, res) {
	Parse.User.logOut();
	res.redirect('/home');
});

// QUIT ROUTE
app.get('/quit', function(req, res) {
	Parse.Cloud.useMasterKey();
	var user = Parse.User.current();

	// Update user status to quit (partial completion)
	user.save({
		status: 'quit'

	// Query user participation
	}).then(function(user) {
		var parQuery = new Parse.Query('Participation');
				parQuery.equalTo('user', user);

		return parQuery.first();

	// Update user participation
	}).then(function(participation) {
		return participation.save({
			completion: 'partial'
		});

	// Redirect to debrief
	}).then(function(user) {
		res.redirect('/debrief');
	}, function(error) {
		res.redirect('/logout');
	});
});

// COMPLETED ROUTE
app.get('/complete', function(req, res) {
	Parse.Cloud.useMasterKey();

	var user = Parse.User.current();

	// Update user status to completed (full completion)
	user.save({
		status: 'completed'

	// Query user participation
	}).then(function(user) {
		var parQuery = new Parse.Query('Participation');
				parQuery.equalTo('user', user);

		return parQuery.first();

	// Update user participation
	}).then(function(participation) {
		return participation.save({
			completion: 'full'
		});

	// Redirect to debrief
	}).then(function() {
		res.redirect('/debrief');
	}, function(error) {
		res.redirect('/logout');
	});
});

// DEBRIEFING ROUTE
app.get('/debrief', function(req, res) {

	var user = Parse.User.current();

	var p1 = user.fetch();
	var p2 = Parse.Session.current();

	Parse.Promise.when(p1, p2).then(function(user, session) {
		var sessionToken;

		if (session) {
			sessionToken = session.getSessionToken();	
		}

		if (user && session) {
			if (user.get('status') !== 'active') {
				user.save({'debriefed': true});
				res.render('business/debrief.ejs', {
					version: _.sample([1,2,3,4,5,6,7,8,9]),
					sessionToken: sessionToken
				});
			} else {
				res.redirect('/');
			}
		} else if (user) {
			if (user.get('status') !== 'active') {
				user.save({'debriefed': true});
				res.render('business/debrief.ejs', {
					version: _.sample([1,2,3,4,5,6,7,8,9])
				});
			} else {
				res.redirect('/');
			}
		} else {
			res.redirect('/');
		}
		

	});

});

app.get('/console', function(req, res) {
	var data = {sessionToken:""};

	if (Parse.User.current()) {
		data.sessionToken = Parse.User.current().getSessionToken();
	}

	res.render('business/console.ejs', data);

});

// TESTING ONLY
app.post('/console', function(req, res) {
	var username = req.body.username;
	var password = req.body.password;

	Parse.User.logIn(username, password).then(function(user) {
		res.redirect('/console');
	});
});

// WILDCARD ROUTES
app.get('*', function(req, res) {
	res.redirect('/');
});

// Attach the Express app to Cloud Code.
app.listen();