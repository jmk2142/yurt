var _ = require('cloud/modules/underscore');

exports = _.extend(exports, {
	// Unique username generator
	getUsername: function() {

		// Fetch list of taken usernames
		var query = new Parse.Query('User');
				query.limit(500);
				query.select('username');

		var usernames;

		return query.find().then(function(users) {
			usernames = _.map(users, function(user) {
				return user.getUsername();
			});

			return Parse.Promise.as(usernames);

		}).then(function(takenUsernames) {

			// Dictionary
			var adjectives = ["yummy","delicious","tasty","sweet","bitter","sour","salty","slippery","slimy","spiky","prickly","smooth","rough","sticky","soft","hard","wet","dry","furry","sad","happy","funny","boring","nasty","naughty","angry","mean","nice","beautiful","pretty","lovely","friendly","grumpy","scary","lonely","loud","noisy","quiet","slow","fast","poor","rich","strong","weak","old","new","young","lazy","sleepy","tired","furry","tall","short","round","fat","long","skinny","thin","thick","smelly","big","little","tiny","small","huge","enormous","gigantic","large","yellow","red","orange","blue","purple","brown","black","white","green","pink"];
			var nouns = ["apple","ball","bed","bear","bell","bird","boat","giant","dinosaur","cake","car","cat","corn","chair","chicken","cow","dog","wind","frog","duck","egg","eye","snail","wave","lizard","foot","cloud","fish","train","flower","pet","book","snake","grass","pie","hand","pizza","orange","bike","horse","house","kitten","leg","letter","ant","tomato","money","tooth","mice","friend","spider","pig","rabbit","rain","ring","clock","fairy","plane","song","sheep","shoe","tree","plant","truck","stick","sun","toy","thing"];

			var adj, noun, username;

			do {
				// Create username
				adj = _.sample(adjectives);
				noun = _.sample(nouns);
				username = adj + '-' + noun;

				// Test username for uniqueness
				unique = !_.contains(takenUsernames, username);

			} while (!unique);

			return Parse.Promise.as(username);
		}, function(error) {
			return Parse.Promise.error('Could not generate unique username.');
		});
	},
	// Requires underscore.js
	getUniqueName: function(options) {
		options = options || {};
		
		var gender = options.gender || undefined;
		var count = options.count || 4;	// Default count is 4

		// Names Lists (40 each)
		var listMale		= _.shuffle(["Liam", "Noah", "Mason", "Ethan", "Logan", "Lucas", "Jackson", "Jacob", "Aiden", "Oliver", "Elijah", "James", "Luke", "Alexander", "Benjamin", "Jack", "Michael", "William", "Daniel", "Gabriel", "Henry", "Carter", "Caleb", "Owen", "Wyatt", "Matthew", "Jayden", "Ryan", "Joshua", "Isaac", "Nathan", "Andrew", "Connor", "Hunter", "Dylan", "David", "Eli", "Sebastian", "Joseph", "Samuel"]);
		var listFemale	= _.shuffle(["Emma", "Olivia", "Sophia", "Ava", "Isabella", "Mia", "Charlotte", "Emily", "Amelia", "Madison", "Harper", "Abigail", "Ella", "Lily", "Chloe", "Avery", "Sofia", "Evelyn", "Grace", "Audrey", "Aubrey", "Zoey", "Elizabeth", "Ellie", "Zoe", "Aria", "Hannah", "Nora", "Scarlett", "Addison", "Brooklyn", "Mila", "Layla", "Natalie", "Lucy", "Lillian", "Claire", "Penelope", "Violet", "Riley"]);
		var listLast		= _.shuffle(["Smith", "Johnson", "Williams", "Brown", "Jones", "Miller", "Davis", "Garcia", "Rodriguez", "Wilson", "Martinez", "Anderson", "Taylor", "Thomas", "Hernandez", "Moore", "Martin", "Jackson", "Thompson", "White", "Lopez", "Lee", "Gonzalez", "Harris", "Clark", "Lewis", "Robinson", "Walker", "Perez", "Hall", "Young", "Allen", "Sanchez", "Wright", "King", "Scott", "Green", "Baker", "Adams", "Nelson"]);
		var nicks = {"Alexander":"Alex","Benjamin":"Ben","Michael":"Mike","William":"Will","Daniel":"Dan","Gabriel":"Gabe","Matthew":"Matt","Joshua":"Josh","Nathan":"Nate","David":"Dave","Joseph":"Joe","Samuel":"Sam","Sophia":"Sophie","Isabella":"Isabel","Abigail":"Abby","Avery":"Avy","Sofia":"Sofie","Evelyn":"Eve","Elizabeth":"Liz","Lillian":"Lily"};

		// Check if valid gender
		if (gender !== "male" && gender !== "female") {
			gender = undefined;
		}
		// Check if valid count
		if (typeof count !== "number") {
			count = 4;
		}
		// Check for max uniques
		var shortestCount = _.min([listMale.length, listFemale.length, listLast.length]);
		if (count > shortestCount) {
			count = shortestCount;
		}

		var names = [];
		// Gender male
		if (gender === "male") {
			_.times(count, function(index) {
				names.push({
					first: listMale[index],
					last: listLast[index],
					nick: _.has(nicks, listMale[index]) ? nicks[listMale[index]] : listMale[index],
					initials: listMale[index].charAt(0) + listLast[index].charAt(0),
					gender: "male"
				});
			});
		// Gender female
		} else if (gender === "female") {
			_.times(count, function(index) {
				names.push({
					first: listFemale[index],
					last: listLast[index],
					nick: _.has(nicks, listFemale[index]) ? nicks[listFemale[index]] : listFemale[index],
					initials: listFemale[index].charAt(0) + listLast[index].charAt(0),
					gender: "female"
				});
			});
		// Gender undefined
		} else {
			_.times(count, function(index) {
				var tmpGender = _.sample(["male", "female"]);
				var firstName = tmpGender === "male" ? listMale[index] : listFemale[index];

				names.push({
					first: firstName,
					last: listLast[index],
					nick: _.has(nicks, firstName) ? nicks[firstName] : firstName,
					initials: firstName.charAt(0) + listLast[index].charAt(0),
					gender: tmpGender
				});
			});
		}

		if (count > 1) {
			return names;	// Array of name objects
		} else {
			return names[0]; // Single name object
		}
	},
	// Fake user role assignment
	assignRoles: function(namesList) {
		var colors = _.shuffle([1,2,3,4,5,6,7,8,9]);
		var limit = 4;

		// Limit to 4 names / roles
		namesList = namesList.slice(0,limit);

		var roles = ["High Critical", "Medium Neutral", "Low Praise", "Prior Subject"];
		var codes = ["CRT", "NEU", "PRA", "SUB"];

		// Returns a name list with research roles designated
		return _.map(namesList, function(name, index) {
			return _.extend(name, {
				code: codes[index],
				color: colors[index]
			});
		});
	},
	// Make profiles of fake users
	makeProfiles: function(condition) {

		var codes = _.shuffle(['CRT','NEU','PRA','SUB']);

		var pitch = "My app is a fun game for language learning. Language learning can be difficult and in this game, people click on different objects in a picture. Each time they click on an object the app \"talks\" and tells the person what the word is in the foreign language. It's like a scavenger hunt and visual story, but fun! So people will be engaged in the learning.";

		var hobbies = _.shuffle([
			"I really enjoy playing soccer. I've been on my local team for six consecutive years.",
			"My main hobby is knitting. My current knitting project is an wool, Irish Aran sweater.",
			"I love movies. I'm trying to finish watching all the Oscar winning films of this decade.",
			"I read books whenever I can. No particular genre, but the classics are always good."
		]);
		var attitudes = _.shuffle([
			"I think it's important to eat healthy. That's why I grow organic vegetables in my own garden.",
			"I try hard to be eco-conscious. If everyone chips in just a little bit, we'd see a huge difference.",
			"I care about social equality. It breaks my heart to see the huge disparities in our society.",
			"I have a soft spot in my heart for animals. I volunteer at a shelter to find puppies good homes."
		]);
		var traits = _.shuffle([
			"People say I'm pretty adventurous. Last year I spent some time hiking the Alaskan wilderness.",
			"Loyalty. Once a friend, I'm the kind of person who will stick with you through thick and thin.",
			"My friends think I'm very humorous. So I guess you can say I'm nobody. \"No Body\" Get it? Ha.",
			"I have a track record for being reliable. Whether it's work or personal, if I promise... I'll deliver."
		]);

		var knowledge = {
			CRT: "I have a Ed.D. in instructional technology from Columbia University and a MFA in digital media from Rhode Island School of Design. I currently work at Microsoft as a project manager for various non-profit initiatives mainly around mobile learning. Currently my pet project is research around app UI interactions and how interaction design constraints affect our cognition, learning.",
			NEU: "I'm a first year M.A. student studying learning technology. My undergraduate work was in psychology at University at Buffalo. I'm also a student teacher now at Boston School of Science and Mathematics. I'm working on my hours to be certified as a technology specialist. I'm particularly interested in helping teachers implement iPads in the classroom to help learning.",
			PRA: "I have a background in computer science from SUNY Albany and I'm currently finishing my 1st year in adult learning (Masters program). Right now I'm a junior instructor at Codespark, an educational startup that teaches industry professionals how to code. I like building things and I'm interested in learning how to design effective learning apps but I still have a lot more to study.",
			SUB: "I just graduated (MA) the TESOL program at Teachers College, Columbia University. I'm going to continue my studies for the M.Ed. for one more year. My future plan is to return home to teach English at a junior college. About technology, unfortunately I really don't know much. But I think it would be really exciting to let kids use smartphone apps for language learning."
		};

		var struggle = {
			CRT: "I dropped out of school once. My advisor told me that I couldn't articulate my thoughts and that my research was shit. Consequently, I thought I couldn't succeed in this field. But after some thought, I knew I loved learning and technology. Everyday I practiced my writing. My writing was still horrible. Slowly (painfully) I kept at it, making more mistakes along the way. But little by little, I discovered tricks and techniques to help me think clearly and communicate it to others. One day I went back to school and showed them what I'd been working on. They invited me back in. I learned that hard work is everything and that it's okay to fail. You don't know what you can really achieve until you do.",
			NEU: "In undergrad, I thought I'd go into medicine but I forgot that I'm bad at biology. To be honest, I burned out and flunked a few courses. I was lost. But, I was lucky in that I ended up working in a hospital with a great group. My peers didn't come from the most prestigious schools. They didn't seem particularly exceptional. But what moved me was how hard they worked. I watched them keep plugging away at things long after others gave up. Including myself. And I watched them go on to successful careers in medicine at Harvard, U. Penn, Cleveland Clinic. Maybe I had it figured all wrong. I eventually found my calling in teaching. But that experience changed my attitudes about success.",
			PRA: "As a child, my dad gave me a TRS-80. It's a primitive computer you can program. When I first tried though, I found it hard. Really hard. I had no clue what I was doing. I kept wishing I had talent for this. In HS, I wished I had spent more time when I was a kid. A friend of mine, was doing all kinds of neat stuff with webpages and in comparison, I felt I missed my chance. In college I gave it another try. My grades weren't the best, but I learned not to worry so much about the grades. Instead, through countless side projects I focused on whether I was actually learning. (Many of which spectacularly failed or didn't go anywhere!) But, I'm proud of the work. Late-nights + coffee = where I am today.",
			SUB: "I always had a knack for English. I enjoyed reading English books, watching English movies, talking at English conversation school. Naturally, TESOL was a match for me. Imagine how I felt when I first came to NY and the taxi driver screams, \"What? What? No. Blah blah. NO! Blah blah!\" I was embarrassed. Apparently my English was not as good as I thought it was. Part of me wanted to go home. And I immediately started hanging out with others from my country. But I started to realize, I wouldn't get better by avoiding it. And experiencing these problems only makes me understand how my students feel. I'm a lot stronger now. And when taxi drivers yell at me, now I yell back!"
		};

		var profiles = [];

		// For each character, create and push their profile
		_.each(codes, function(code, index) {
			var profile = {
				code: code,
				personal: {
					hobby: hobbies[index],
					attitude: attitudes[index],
					trait: traits[index]
				},
				knowledge: knowledge[code]
			};

			// Add pitch if the character is SUB
			if (code === 'SUB') {
				profile.pitch = pitch;
			}

			// if condition is struggle...
			if (condition === 'struggle') {
				profile.struggle = struggle[code];
			}

			profiles.push(profile);
		});

		return profiles;
	},
	// Returns a semi-randomized array of question IDs in order they should be presented.
	generateOrder: function(questions) {

		// Batteries
		var demographic, measureA, decision, measureB;

		// Sorted demographic items
		demographic = _.chain(questions).filter(function(question) {
			return question.get('group') === 'demographic';
		}).sortBy(function(question) {
			return question.get('order');
		}).value();

		measureA = _.chain(questions).filter(function(question) {
			return _.contains(question.get('battery'), 'measureA');
		}).groupBy(function(question) {
			if (question.get('group') === 'theory') {
				return _.contains(question.get('category'), 'intelligence') ? 'theoryINT' : 'theoryPER';
			} else {
				return question.get('group');
			}
		}).toArray().shuffle().map(function(qSet, index, list) {
			// Check first of questionSet to see if order is 0;
			if (_.first(qSet).get('order') === 0) {
				// list[index] = _.shuffle(qSet);
				return _.shuffle(qSet);
			} else {
				return _.sortBy(qSet, function(question) {
					return question.get('order');
				});
				// list[index] = _.sortBy(qSet, function(question) {
				// 	return question.get('order');
				// });
			}
		}).value();


		// .each(function(qSet, index, list) {
		// 	// Check first of questionSet to see if order is 0;
		// 	if (_.first(qSet).get('order') === 0) {
		// 		list[index] = _.shuffle(qSet);
		// 	} else {
		// 		list[index] = _.sortBy(qSet, function(question) {
		// 			return question.get('order');
		// 		});
		// 	}
		// }).value();

		// .sortBy(function(set) {
		// 	var sample = _.first(set);
		// 	var group = sample.get('group');
		// 	var category = _.contains(sample.get('category'), 'intelligence') ? 'theoryINT' : 'theoryPER';

		// 	// if (group === 'theory') {
		// 	// 	if (category === 'theoryINT') {
		// 	// 		return 1;
		// 	// 	} else {
		// 	// 		return 2;
		// 	// 	}
		// 	// } else {
		// 	// 	return 3;
		// 	// }

		// 	return 1;

		// }).value();

		decision = _.chain(questions).filter(function(question) {
			return question.get('group') === 'connection';
		}).sortBy(function(question) {
			return question.get('order');
		}).value();

		// measureB = _.chain(questions).filter(function(question) {
		// 	return _.contains(question.get('battery'), 'measureB');
		// }).groupBy(function(question) {
		// 	if (question.get('group') === 'uncertainty') {
		// 		return _.contains(question.get('category'), 'selected') ? 'uncertaintyS' : 'uncertaintyB';
		// 	} else if (question.get('group') === 'information') {
		// 		return _.contains(question.get('category'), 'selected') ? 'informationS' : 'informationB';
		// 	} else {
		// 		return question.get('group');
		// 	}
		// }).toArray().shuffle().each(function(qSet, index, list) {
		// 	// Check first of questionSet to see if order is 0;
		// 	if (_.first(qSet).get('order') === 0) {
		// 		list[index] = _.shuffle(qSet);
		// 	} else {
		// 		list[index] = _.sortBy(qSet, function(question) {
		// 			return question.get('order');
		// 		});
		// 	}
		// }).sortBy(function(set) {
		// 	var sample = _.first(set);
		// 	var group = sample.get('group');
		// 	var category = _.contains(sample.get('category'), 'selected') ? 'selected' : 'blocked';

		// 	if (group === 'uncertainty') {
		// 		if (category === 'selected') {
		// 			return 1;
		// 		} else {
		// 			return 2;
		// 		}
		// 		// return category === 'selected' ? 1.0 : 1.1;
		// 	} else if (group === 'information') {
		// 		if (category === 'selected') {
		// 			return 3;
		// 		} else {
		// 			return 4;
		// 		}
		// 		// return category === 'selected' ? 2.0 : 2.1;
		// 	} else {
		// 		return 5;
		// 	}

		// }).value();

		measureB = _.chain(questions).filter(function(question) {
			return _.contains(question.get('battery'), 'measureB');
		}).groupBy(function(question) {
			if (question.get('group') === 'uncertainty') {
				return _.contains(question.get('category'), 'selected') ? 'uncertaintyS' : 'uncertaintyB';
			} else if (question.get('group') === 'information') {
				return _.contains(question.get('category'), 'selected') ? 'informationS' : 'informationB';
			} else if (question.get('group') === 'theory') {
				return _.contains(question.get('category'), 'intelligence') ? 'theoryINT' : 'theoryPER';
			} else {
				return question.get('group');
			}
		}).toArray().shuffle().map(function(qSet, index, list) {
			// Check first of questionSet to see if order is 0;
			// if (_.first(qSet).get('order') === 0) {
			// 	list[index] = _.shuffle(qSet);
			// } else {
			// 	list[index] = _.sortBy(qSet, function(question) {
			// 		return question.get('order');
			// 	});
			// }

			// Check first of questionSet to see if order is 0;
			if (_.first(qSet).get('order') === 0) {
				return _.shuffle(qSet);
			} else {
				return _.sortBy(qSet, function(question) {
					return question.get('order');
				});
			}
		}).sortBy(function(set) {
			var sample = _.first(set);
			var group = sample.get('group');
			var category = _.contains(sample.get('category'), 'selected') ? 'selected' : 'blocked';

			if (group === 'uncertainty') {
				if (category === 'selected') {
					return 1;
				} else {
					return 2;
				}
				// return category === 'selected' ? 1.0 : 1.1;
			} else if (group === 'information') {
				if (category === 'selected') {
					return 3;
				} else {
					return 4;
				}
				// return category === 'selected' ? 2.0 : 2.1;
			} else {
				return 5;
			}

		}).value();

		// .each(function(qSet, index, list) {
		// 	// Check first of questionSet to see if order is 0;
		// 	if (_.first(qSet).get('order') === 0) {
		// 		list[index] = _.shuffle(qSet);
		// 	} else {
		// 		list[index] = _.sortBy(qSet, function(question) {
		// 			return question.get('order');
		// 		});
		// 	}
		// })



		var questionIDs = {
			demographic: _.chain(demographic).flatten().pluck('id').value(),
			measureA: _.chain(measureA).flatten().pluck('id').value(),
			decision: _.chain(decision).flatten().pluck('id').value(),
			measureB: _.chain(measureB).flatten().pluck('id').value()
		};
		
		return questionIDs;
	},
	/*
		PARAMS: user and options{reset} that indicates
		whether or not to destroy the existing config if found
		defaults to false.
		RETURN: Parse.Promise <Config>
	*/
	makeConfig: function(user, options) {

		options = options ? options : {reset: false};
		var reset = options.reset;

		// Determine if we need to reset
		// reset = (typeof reset === 'boolean') ? reset : false; // default false

		// Check for a user config object
		var Configuration = Parse.Object.extend('Configuration');
		var queryConfig = new Parse.Query('Configuration');
				queryConfig.equalTo('user', user);

		var promise = queryConfig.first().then(function(config) {

			var p1, p2, p3;
			var condition, quesQuery;

			// Config NOT FOUND
			if (!config) {

				condition = _.sample(['control','struggle']);

				quesQuery = new Parse.Query('Question');
				quesQuery.select(['objectId','battery','group','category','order']);

				// Select questions based on struggle
				if (condition === 'struggle') {
					quesQuery.containedIn('condition', ['both','struggle']);

				// Select questions based on control
				} else {
					quesQuery.equalTo('condition', 'both');
				}

				p1 = quesQuery.find();
				p2 = Parse.Promise.as(condition);
				p3 = Parse.Promise.as(undefined);

				return Parse.Promise.when(p1, p2);

			// FOUND config
			} else {

				if (reset) {
					condition = _.sample(['control','struggle']);

					quesQuery = new Parse.Query('Question');
					quesQuery.select(['objectId','battery','group','category','order']);

					// Select questions based on struggle
					if (condition === 'struggle') {
						quesQuery.containedIn('condition', ['both','struggle']);

					// Select questions based on control
					} else {
						quesQuery.equalTo('condition', 'both');
					}

					p1 = quesQuery.find();
					p2 = Parse.Promise.as(condition);
					p3 = Parse.Promise.as(config);

				} else {

					p1 = Parse.Promise.as(undefined);
					p2 = Parse.Promise.as(undefined);
					p3 = Parse.Promise.as(config);

				}

			}

			return Parse.Promise.when(p1, p2, p3);

		}).then(function(questions, condition, config) {

			// Config NOT FOUND - CREATE ONE
			if (!config) {

				// CREATE THE CONFIGURATION OBJECT
				config = new Configuration({
					user: user,
					condition: condition,
					progress: 'start',
					conversation: true,
					battery: exports.generateOrder(questions),
					characters: exports.assignRoles(exports.getUniqueName()),
					profiles: exports.makeProfiles(condition),
					panelistOrder: _.shuffle(['CRT','NEU','PRA'])
				});

				var acl = new Parse.ACL(user);
						acl.setRoleReadAccess('admin', true);
						acl.setRoleWriteAccess('admin', true);

				config.setACL(acl);

				return config.save();

			// FOUND config
			} else {

				// SET THE CONFIGURATION OBJECT
				if (reset) {
					config.set({
						user: user,
						condition: condition,
						progress: 'start',
						conversation: true,
						battery: exports.generateOrder(questions),
						characters: exports.assignRoles(exports.getUniqueName()),
						profiles: exports.makeProfiles(condition),
						panelistOrder: _.shuffle(['CRT','NEU','PRA'])
					});

					return config.save();

				} else {

					return Parse.Promise.as(config);

				}
				
			}

		}, function(error) {
			return Parse.Promise.error('Problem making Config');
		});

		return promise;
	},
	/*
		PARAMS: user or userID
		RETURN: Parse.Promise <User>
	*/
	resetUser: function(user, options) {

		options = options ? options : {resetConfig: false};

		var userPromise;

		if (typeof user === 'string') {
			
			var userQuery = new Parse.Query('User');	
			userPromise = userQuery.first();

		} else {

			userPromise = Parse.Promise.as(user);

		}

		userPromise.then(function(user) {
			return user.save({
				status: 'active',
				debriefed: false
			});
		}, function(error) {
			return Parse.Promise.error('Problem reseting User');
		});

		return userPromise;
	}
});

