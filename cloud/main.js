require('cloud/app.js');
var _ = require('cloud/modules/underscore');

var base = require('cloud/modules/baseFunctions.js');
var getUsername = base.getUsername;
var getUniqueName = base.getUniqueName;
var assignRoles = base.assignRoles;
var makeProfiles = base.makeProfiles;
var generateOrder = base.generateOrder;
var makeConfig = base.makeConfig;
var resetUser = base.resetUser;

// PROTECTED USER IDs
var protectedIDs = []; // jmk2142


Parse.Cloud.afterSave('Log', function(request) {
	
	var log = request.object;

	if (log.existed() && (log.get('noise') === undefined)) {
		// Check if there is some change. If not, for now mark as noise.
		var validLog = false;
		var validDuration = (log.get('duration') || log.get('idleTime') || log.get('activeTime'));
		var validLength = Math.abs(log.get('lengthChange'));

		if (validDuration || validLength) {
			validLog = true;
		}

		// DESTROY if it is useless
		if (!validLog) {
			log.destroy();
		}
	}

});


Parse.Cloud.beforeSave(Parse.User, function(request, response) {
	var acl = new Parse.ACL(request.user);
			acl.setPublicReadAccess(false);
			acl.setRoleReadAccess('admin', true);

	request.object.setACL(acl);

	response.success(request.object);
});

Parse.Cloud.define("getUsername", function(request, response) {
	Parse.Cloud.useMasterKey();

	// Fetch list of taken usernames
	var query = new Parse.Query('User');
			query.limit(500);
			query.select('username');

	var usernames;

	getUsername().then(function(username) {
		response.success(username);
	}, function(error) {
		response.error(error.message);
	});

	// query.find().then(function(users) {
	// 	usernames = _.map(users, function(user) {
	// 		return user.getUsername();
	// 	});

	// 	return Parse.Promise.as(usernames);

	// }).then(function(takenUsernames) {

	// 	// Dictionary
	// 	var adjectives = ["yummy","delicious","tasty","sweet","bitter","sour","salty","slippery","slimy","spiky","prickly","smooth","rough","sticky","soft","hard","wet","dry","furry","sad","happy","funny","boring","nasty","naughty","angry","mean","nice","beautiful","pretty","lovely","friendly","grumpy","scary","lonely","loud","noisy","quiet","slow","fast","poor","rich","strong","weak","old","new","young","lazy","sleepy","tired","furry","tall","short","round","fat","long","skinny","thin","thick","smelly","big","little","tiny","small","huge","enormous","gigantic","large","yellow","red","orange","blue","purple","brown","black","white","green","pink"];
	// 	var nouns = ["apple","ball","bed","bear","bell","bird","boat","giant","dinosaur","cake","car","cat","corn","chair","chicken","cow","dog","wind","frog","duck","egg","eye","snail","wave","lizard","foot","cloud","fish","train","flower","pet","book","snake","grass","pie","hand","pizza","orange","bike","horse","house","kitten","leg","letter","ant","tomato","money","tooth","mice","friend","spider","pig","rabbit","rain","ring","clock","fairy","plane","song","sheep","shoe","tree","plant","truck","stick","sun","toy","thing"];

	// 	var adj, noun, username;

	// 	do {
	// 		// Create username
	// 		adj = _.sample(adjectives);
	// 		noun = _.sample(nouns);
	// 		username = adj + '-' + noun;

	// 		// Test username for uniqueness
	// 		unique = !_.contains(takenUsernames, username);

	// 	} while (!unique);

	// 	response.success(username);
	// });

});

Parse.Cloud.define('makeConfig', function(request, response) {
	var user = Parse.User.current();

	// Reset the user and make the new Config
	makeConfig(user, {reset: true}).then(function(config) {
		return resetUser(user);
	}).then(function(user) {
		response.success(user);
	}, function(error) {
		response.error(error.message);
	});
});

Parse.Cloud.define('saveReferral', function(request, response) {
	Parse.Cloud.useMasterKey();

	var user = request.user;
	var referralSource = request.params.refSource;

	var query = new Parse.Query('Referral');
			query.equalTo('user', user);

	query.first().then(function(referral) {

		// Create a new referral object if one does not exist
		if (!referral) {
			var Referral = Parse.Object.extend('Referral');
			referral = new Referral();
			var acl = new Parse.ACL();
			acl.setRoleReadAccess('admin', true);
			acl.setRoleWriteAccess('admin', true);
			referral.setACL(acl);
		}

		// Save the referral object
		referral.save({
			user: user,
			source: referralSource
		}).then(function(referral) {
			response.success(referral);
		}, function(error) {
			response.error(error.message);
		});

	}, function(error) {
		response.error(error.message);
	});
});

Parse.Cloud.define('vote', function(request, response) {
	Parse.Cloud.useMasterKey();

	var user = request.user;
	var valence = request.params.valence;
	var postID = request.params.postID;

	var postQuery = new Parse.Query('Post');
			postQuery.equalTo('objectId', postID);

	postQuery.first().then(function(post) {
		if (valence === 'up') {
			post.addUnique('upvotes', user);
			post.remove('downvotes', user);
		} else if (valence === 'down') {
			post.remove('upvotes', user);
			post.addUnique('downvotes', user);
		} else {
			post.remove('upvotes', user);
			post.remove('downvotes', user);
		}

		return post.save();
	}).then(function(post) {
		response.success(post);
	}, function(error) {
		response.error(error.message);
	});

});

// DESTROYS user and all user artifacts
// params = array of userIDs
Parse.Cloud.define('destroyUsers', function(request, response) {
	var user = Parse.User.current();
	var destroyIDs = request.params.userIDs;

	var roleQuery = new Parse.Query(Parse.Role);
			roleQuery.equalTo('name','admin');
			roleQuery.equalTo('users', Parse.User.current());

	roleQuery.first().then(function(adminRole){

	  if (adminRole) {
	  	return Parse.Promise.as(destroyIDs);
	  } else {
	  	response.error('HAL9000: I\'m sorry Dave, I\'m afraid I can\'t do that.');
	  }

	// Generate empty users
	}).then(function(destroyIDs) {
		var users = [];
		_.each(destroyIDs, function(id) {
			var user = Parse.User.createWithoutData(id);
			users.push(user);
		});

		return Parse.Promise.as(users);

	// FIND votes
	}).then(function(users) {

		var pUpQuery = new Parse.Query('Post');
				pUpQuery.containedIn('upvotes', users);
		var pDownQuery = new Parse.Query('Post');
				pDownQuery.containedIn('downvotes', users);

		var postQuery = Parse.Query.or(pUpQuery, pDownQuery);

		var p1 = Parse.Promise.as(users);
		var p2 = postQuery.find();

		return Parse.Promise.when(p1, p2);

	// REMOVE votes
	}).then(function(users, posts) {

		// For each post...
		_.each(posts, function(post) {
			// Remove each user...
			_.each(users, function(user) {
				post.remove('upvotes', user);
				post.remove('downvotes', user);
			});
		});

		var p1 = Parse.Promise.as(users);
		var p2 = Parse.Object.saveAll(posts);

		return Parse.Promise.when(p1, p2);

	// FIND comments
	}).then(function(users) {
		var p1 = Parse.Promise.as(users);

		var query = new Parse.Query('Comment');
				query.containedIn('user', users);
				query.limit(1000);
		
		var p2 = query.find();

		return Parse.Promise.when(p1, p2);

	// DESTROY comments
	}).then(function(users, comments) {
		var p1 = Parse.Promise.as(users);
		var p2 = Parse.Object.destroyAll(comments);

		return Parse.Promise.when(p1, p2);

	// FIND logs
	}).then(function(users) {
		var p1 = Parse.Promise.as(users);

		var query = new Parse.Query('Log');
				query.containedIn('user', users);
				query.limit(1000);

		var p2 = query.find();

		return Parse.Promise.when(p1, p2);

	// DESTROY logs
	}).then(function(users, logs) {
		var p1 = Parse.Promise.as(users);
		var p2 = Parse.Object.destroyAll(logs);

		return Parse.Promise.when(p1, p2);

	// FIND profile
	}).then(function(users) {
		var p1 = Parse.Promise.as(users);

		var query = new Parse.Query('Profile');
				query.containedIn('user', users);
				query.limit(1000);

		var p2 = query.find();

		return Parse.Promise.when(p1, p2);

	// DESTROY profiles
	}).then(function(users, logs) {
		var p1 = Parse.Promise.as(users);
		var p2 = Parse.Object.destroyAll(logs);

		return Parse.Promise.when(p1, p2);

	// FIND configuration
	}).then(function(users) {
		var p1 = Parse.Promise.as(users);

		var query = new Parse.Query('Configuration');
				query.containedIn('user', users);
				query.limit(1000);

		var p2 = query.find();

		return Parse.Promise.when(p1, p2);

	// DESTROY configurations
	}).then(function(users, configs) {
		var p1 = Parse.Promise.as(users);
		var p2 = Parse.Object.destroyAll(configs);

		return Parse.Promise.when(p1, p2);

	// FIND responses
	}).then(function(users) {
		var p1 = Parse.Promise.as(users);

		var query = new Parse.Query('Response');
				query.containedIn('user', users);
				query.limit(1000);

		var p2 = query.find();

		return Parse.Promise.when(p1, p2);

	// DESTROY responses
	}).then(function(users, responses) {
		var p1 = Parse.Promise.as(users);
		var p2 = Parse.Object.destroyAll(responses);

		console.log('responses about to be destroyed');

		return Parse.Promise.when(p1, p2);

	// DESTROY users
	}).then(function(users) {
		console.log('users about to be destroyed');
		Parse.Cloud.useMasterKey();
		return Parse.Object.destroyAll(users);
	}).then(function() {
		response.success('Destroyed All User Objects');
	}, function(error) {
		response.error('Error in promise chain');
	});

});

// Protect JMK2142
Parse.Cloud.beforeDelete(Parse.User, function(request, response) {
	var user = request.object;
	if (_.contains(protectedIDs, user.id)) {
		console.log('Tried to delete protected user.');
		response.error('Can not delete user');
	} else {
		response.success(request.object);
	}
});

// Protect JMK2142
Parse.Cloud.beforeDelete('Configuration', function(request, response) {
	var config = request.object;

	var nullUser = config.get('user') === null;
	var protectedUser = _.contains(protectedIDs, config.get('user').id);

	if (_.contains(protectedIDs, config.get('user').id) && !request.master) {
		console.log('Tried to delete protected config.');
		response.error('Can not delete config');
	} else {
		console.log('destroying configuration:');
		response.success(request.object);
	}
});