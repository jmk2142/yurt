/*
	PARAMS: user and options{reset} that indicates
	whether or not to destroy the existing config if found
	defaults to false.

	RETURN: Parse.Promise <Config>
*/

function makeConfig(user, options) {

	options = options ? options : {reset: false};
	var reset = options.reset;

	// Determine if we need to reset
	reset = (typeof reset === 'boolean') ? reset : false; // default false

	// Check for a user config object
	var Configuration = Parse.Object.extend('Configuration');
	var queryConfig = new Parse.Query('Configuration');
			queryConfig.equalTo('user', user);

	var promise = queryConfig.first().then(function(config) {

		if (reset) {
			return config.destroy({
				useMasterKey: true
			});
		} else {
			return Parse.Promise.as(config);
		}

	}).then(function(config) {

		var p1, p2, p3;

		// Config NOT FOUND
		if (!config) {

			var condition = _.sample(['control','struggle']);

			var quesQuery = new Parse.Query('Question');
					quesQuery.select(['objectId','battery','group','category','order']);

			// Select questions based on struggle
			if (condition === 'struggle') {
				quesQuery.containedIn('condition', ['both','struggle']);

			// Select questions based on control
			} else {
				quesQuery.equalTo('condition', 'both');
			}

			p1 = quesQuery.find();
			p2 = Parse.Promise.as(condition);
			p3 = Parse.Promise.as(undefined);

			return Parse.Promise.when(p1, p2);

		// FOUND config
		} else {

			p1 = Parse.Promise.as(undefined);
			p2 = Parse.Promise.as(undefined);
			p3 = Parse.Promise.as(config);
		
		}

		return Parse.Promise.when(p1, p2, p3);

	}).then(function(questions, condition, config) {

		// Config NOT FOUND - CREATE ONE
		if (!config) {

			// CREATE THE CONFIGURATION OBJECT
			config = new Configuration({
				user: user,
				condition: condition,
				progress: 'start',
				conversation: true,
				battery: generateOrder(questions),
				characters: assignRoles(getUniqueName()),
				profiles: makeProfiles(condition),
				panelistOrder: _.shuffle(['CRT','NEU','PRA'])
			});

			var acl = new Parse.ACL(user);
					acl.setRoleReadAccess('admin', true);
					acl.setRoleWriteAccess('admin', true);

			config.setACL(acl);

			return config.save();

		// FOUND config
		} else {
			return Parse.Promise.as(config);
		}

	}, function(error) {
		return Parse.Promise.error('Problem making Config');
	});

	return promise;
}

/*
	PARAMS: user or userID
	RETURN: Parse.Promise <User>
*/
function resetUser(user, options) {

	options = options ? options : {resetConfig: false};

	var userPromise;

	if (typeof user === 'string') {
		
		var userQuery = new Parse.Query('User');	
		userPromise = userQuery.first();

	} else {

		userPromise = Parse.Promise.as(user);

	}

	userPromise.then(function(user) {
		return user.save({
			status: 'active',
			debriefed: false
		});
	}, function(error) {
		return Parse.Promise.error('Problem reseting User');
	});

	return userPromise;
}


resetUser('someID').then(function(user) {
	return makeConfig(user, {reset: true});
}).then(function(config) {
	response.success(config);
}, function(error) {
	response.error(error.message);
});