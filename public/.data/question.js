// Demographic questions
// This series has ORDER
var demographic = [
	{
		name: 'demoGen',
		condition: 'both',
		group: 'demographic',
		order: 1,
		battery: ['demographic'],
		category: [],
		primary: {
			type: 'multichoice',
			question: 'What is your gender?',
			responses: ['Male','Female'],
			codes: ['M','F'],
			scores: [1,2]
		}
	},
	{
		name: 'demoAge',
		condition: 'both',
		group: 'demographic',
		order: 2,
		battery: ['demographic'],
		category: [],
		primary: {
			type: 'multichoice',
			question: 'Age: What is your age?',
			responses: [
				'17 years or under',
				'18-24 years old',
				'25-34 years old',
				'35-44 years old',
				'45-54 years old',
				'55-64 years old',
				'65-74 years old',
				'75 years or older'
			],
			codes: ['Under','18-24','25-34','35-44','45-54','55-64','65-74','75+'],
			scores: [0,1,2,3,4,5,6,7]
		}
	},
	{
		name: 'demoEdu',
		condition: 'both',
		group: 'demographic',
		order: 3,
		battery: ['demographic'],
		category: [],
		primary: {
			type: 'multichoice',
			question: 'What is the highest degree or level of school you have completed? If currently enrolled, highest degree received.',
			responses: [
				'No schooling completed',
				'Nursery school to 8th grade',
				'Some high school, no diploma',
				'High school graduate, diploma or the equivalent (for example: GED)',
				'Some college credit, no degree',
				'Trade/technical/vocational training',
				'Associate degree',
				'Bachelor’s degree',
				'Master’s degree',
				'Professional degree',
				'Doctorate degree'
			],
			codes: ['NS','SM','SH','HS','SC','TR','AS','BA','MA','PR','DR'],
			scores: [0,1,2,3,4,5,6,7,8,9,10]
		}
	},
	{
		name: 'demoLang',
		condition: 'both',
		group: 'demographic',
		order: 4,
		battery: ['demographic'],
		category: [],
		primary: {
			type: 'multichoice',
			question: 'Is English your native language?',
			responses: ['Yes', 'No'],
			codes: ['Y','N'],
			scores: [1,0]
		}
	},
	{
		name: 'demoTech',
		condition: 'both',
		group: 'demographic',
		order: 5,
		battery: ['demographic'],
		category: [],
		primary: {
			type: 'multichoice',
			question: 'How often do you communicate with others using internet based, social applications and networks? (e.g. Facebook, Twitter, Skype, WeChat, WhatsApp, etc.)',
			responses: [
				'Several times a day',
				'At least once a day',
				'Several times a week',
				'At least once a week',
				'Several times a month',
				'At least once a month',
				'Less than once a month'
			],
			codes: ['dayMany','dayOnce','weekMany','weekOnce','monthMany','monthOnce','monthRare'],
			scores: [7,6,5,4,3,2,1]
		}
	}
];

// Measure A
// This series has ORDER
var theoryIntelligence = [
	{
		name: 'intA',
		condition: 'both',
		group: 'theory',
		order: 1,
		battery: ['measureA','measureB'],
		category: ['intelligence'],
		primary: {
			type: 'likert',
			question: 'You have a certain amount of intelligence and you can\'t really do much to change it.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1,2,3,4,5,6]
		}
	},
	{
		name: 'intB',
		condition: 'both',
		group: 'theory',
		order: 2,
		battery: ['measureA','measureB'],
		category: ['intelligence'],
		primary: {
			type: 'likert',
			question: 'Your intelligence is something about you that you can\'t change very much.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1,2,3,4,5,6]
		}
	},
	{
		name: 'intC',
		condition: 'both',
		group: 'theory',
		order: 3,
		battery: ['measureA','measureB'],
		category: ['intelligence'],
		primary: {
			type: 'likert',
			question: 'To be honest, you can\'t really change how intelligent you are.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1,2,3,4,5,6]
		}
	},
	{
		name: 'intD',
		condition: 'both',
		group: 'theory',
		order: 4,
		battery: ['measureA','measureB'],
		category: ['intelligence'],
		primary: {
			type: 'likert',
			question: 'You can learn new things, but you can\'t really change your basic intelligence.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1,2,3,4,5,6]
		}
	}
];

// This series has ORDER
var theoryPersonality = [
	{
		name: 'perA',
		condition: 'both',
		group: 'theory',
		order: 1,
		battery: ['measureA','measureB'],
		category: ['personality'],
		primary: {
			type: 'likert',
			question: 'The kind of person you are, is something very basic about you and it can\'t be changed very much.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1,2,3,4,5,6]
		}
	},
	{
		name: 'perB',
		condition: 'both',
		group: 'theory',
		order: 2,
		battery: ['measureA','measureB'],
		category: ['personality'],
		primary: {
			type: 'likert',
			question: 'People can do things differently, but the important parts of who you are can\'t really be changed.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1,2,3,4,5,6]
		}
	},
	{
		name: 'perC',
		condition: 'both',
		group: 'theory',
		order: 3,
		battery: ['measureA','measureB'],
		category: ['personality'],
		primary: {
			type: 'likert',
			question: 'As much as I hate to admit it, you can\'t teach an old dog new tricks. You can\'t really change your deepest attributes.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1,2,3,4,5,6]
		}
	},
	{
		name: 'perD',
		condition: 'both',
		group: 'theory',
		order: 4,
		battery: ['measureA','measureB'],
		category: ['personality'],
		primary: {
			type: 'likert',
			question: 'You are a certain kind of person, and there is not much that can be done to really change that.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1,2,3,4,5,6]
		}
	}
];

// This series has ORDER
var confidenceIntelligence = [
	{
		name: 'confIntA',
		condition: 'both',
		group: 'confidence',
		order: 1,
		battery: ['measureA','measureB'],
		category: ['intelligence'],
		primary: {
			type: 'multichoice',
			question: 'Check the sentence that is most true for you.',
			responses: [
				'I usually think I\'m intelligent',
				'I wonder if I\'m intelligent'
			],
			codes: ['CONF','NOT'],
			scores: [3, -4]
		},
		secondary: {
			type: 'likert',
			question: 'Now show how true the statement you chose is for you.',
			responses: [
				'Very true for me',
				'True for me',
				'Sort of true for me'
			],
			codes: ['VT','T','ST'],
			scores: [3, 2, 1]
		}
	},
	{
		name: 'confIntB',
		condition: 'both',
		group: 'confidence',
		order: 2,
		battery: ['measureA','measureB'],
		category: ['intelligence'],
		primary: {
			type: 'multichoice',
			question: 'Check the sentence that is most true for you.',
			responses: [
				'When presented with something I don\'t know, I\'m usually sure I will be able to learn it.',
				'When presented with something I don\'t know, I often think I may not be able to learn it.'
			],
			codes: ['CONF','NOT'],
			scores: [3, -4]
		},
		secondary: {
			type: 'likert',
			question: 'Now show how true the statement you chose is for you.',
			responses: [
				'Very true for me',
				'True for me',
				'Sort of true for me'
			],
			codes: ['VT','T','ST'],
			scores: [3, 2, 1]
		}
	},
	{
		name: 'confIntC',
		condition: 'both',
		group: 'confidence',
		order: 3,
		battery: ['measureA','measureB'],
		category: ['intelligence'],
		primary: {
			type: 'multichoice',
			question: 'Check the sentence that is most true for you.',
			responses: [
				'I\'m not very confident about my intellectual ability',
				'I feel pretty confident about my intellectual ability'
			],
			codes: ['NOT','CONF'],
			scores: [-4, 3]
		},
		secondary: {
			type: 'likert',
			question: 'Now show how true the statement you chose is for you.',
			responses: [
				'Very true for me',
				'True for me',
				'Sort of true for me'
			],
			codes: ['VT','T','ST'],
			scores: [3, 2, 1]
		}
	}
];

// This series has ORDER
var confidencePersonality = [
	{
		name: 'confPerA',
		condition: 'both',
		group: 'confidence',
		order: 1,
		battery: ['measureA','measureB'],
		category: ['personality'],
		primary: {
			type: 'multichoice',
			question: 'Check the sentence that is most true for you.',
			responses: [
				"When I meet new people, I\'m not sure if they will like me.",
				"When I meet new people, I am sure that they will like me."
			],
			codes: ['NOT','CONF'],
			scores: [-4, 3]
		},
		secondary: {
			type: 'likert',
			question: 'Now show how true the statement you chose is for you.',
			responses: [
				'Very true for me',
				'True for me',
				'Sort of true for me'
			],
			codes: ['VT','T','ST'],
			scores: [3, 2, 1]
		}
	},
	{
		name: 'confPerB',
		condition: 'both',
		group: 'confidence',
		order: 2,
		battery: ['measureA','measureB'],
		category: ['personality'],
		primary: {
			type: 'multichoice',
			question: 'Check the sentence that is most true for you.',
			responses: [
				"If I were another person choosing a friend, I am not sure I would choose me.",
				"If I were another person choosing a friend, I am sure I would choose me."
			],
			codes: ['NOT','CONF'],
			scores: [-4, 3]
		},
		secondary: {
			type: 'likert',
			question: 'Now show how true the statement you chose is for you.',
			responses: [
				'Very true for me',
				'True for me',
				'Sort of true for me'
			],
			codes: ['VT','T','ST'],
			scores: [3, 2, 1]
		}
	},
	{
		name: 'confPerC',
		condition: 'both',
		group: 'confidence',
		order: 3,
		battery: ['measureA','measureB'],
		category: ['personality'],
		primary: {
			type: 'multichoice',
			question: 'Check the sentence that is most true for you.',
			responses: [
				"I\'m pretty sure people like my personality",
				"I\'m not pretty sure people like my personality"
			],
			codes: ['CONF','NOT'],
			scores: [3, -4]
		},
		secondary: {
			type: 'likert',
			question: 'Now show how true the statement you chose is for you.',
			responses: [
				'Very true for me',
				'True for me',
				'Sort of true for me'
			],
			codes: ['VT','T','ST'],
			scores: [3, 2, 1]
		}
	}
];

// This series has ORDER
var goalOrientation = [
	{
		name: 'goalA',
		condition: 'both',
		group: 'goals',
		order: 1,
		battery: ['measureA','measureB'],
		category: [],
		primary: {
			type: 'likert',
			question: 'If I knew I wasn\'t knowledgeable about a discussion, I probably wouldn\'t participate even if I might learn a lot from it.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1, 2, 3, 4, 5, 6]
		}
	},
	{
		name: 'goalB',
		condition: 'both',
		group: 'goals',
		order: 2,
		battery: ['measureA','measureB'],
		category: [],
		primary: {
			type: 'likert',
			question: 'Although I hate to admit it, I sometimes would rather perform well in a discussion than learn a lot.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [1, 2, 3, 4, 5, 6]
		}
	},
	{
		name: 'goalC',
		condition: 'both',
		group: 'goals',
		order: 3,
		battery: ['measureA','measureB'],
		category: [],
		primary: {
			type: 'likert',
			question: 'It\'s much more important for me to learn things in discussions than it is to perform the best.',
			responses: ['Strongly Agree','Agree','Mostly Agree','Mostly Disagree','Disagree','Strongly Disagree'],
			codes: ['SA','A','MA','MD','D','SD'],
			scores: [6, 5, 4, 3, 2, 1]
		}
	},
	{
		name: 'goalD',
		condition: 'both',
		group: 'goals',
		order: 4,
		battery: ['measureA','measureB'],
		category: [],
		primary: {
			type: 'multichoice',
			question: 'If I had to choose between performing well and being challenged in discussions, I would choose ...',
			responses: [
				"Perform well",
				"Being challenged"
			],
			codes: ['PERFORM','LEARN'],
			scores: [1, 6]
		}
	}
];

// This series can be RANDOMIZED
var background = [
	{
		name: 'knowTech',
		condition: 'both',
		group: 'background',
		order: 0,
		battery: ['measureA','measureB'],
		category: ['knowledge','technology','design'],
		primary: {
			type: 'likert',
			question: 'Please rate how knowledgeable you are about the domain of technology & design.',
			responses: [
				'Unknowledgable',
				'Slightly Knowledgable',
				'Moderately Knowledgable',
				'Knowledgable',
				'Very Knowledgeable',
			],
			codes: ['U','SK','MK','K','VK'],
			scores: [1,2,3,4,5]
		}
	},
	{
		name: 'knowLearn',
		condition: 'both',
		group: 'background',
		order: 0,
		battery: ['measureA','measureB'],
		category: ['knowledge','learning'],
		primary: {
			type: 'likert',
			question: 'Please rate how knowledgeable you are about the domain of learning.',
			responses: [
				'Unknowledgable',
				'Slightly Knowledgable',
				'Moderately Knowledgable',
				'Knowledgable',
				'Very Knowledgeable',
			],
			codes: ['U','SK','MK','K','VK'],
			scores: [1,2,3,4,5]
		}
	},
	{
		name: 'interTech',
		condition: 'both',
		group: 'background',
		order: 0,
		battery: ['measureA','measureB'],
		category: ['interest','technology','design'],
		primary: {
			type: 'likert',
			question: 'Please rate how interested you are about the domain of technology & design.',
			responses: [
				'Uninterested',
				'Slightly Interested',
				'Moderately Interested',
				'Interested',
				'Very Interested'
			],
			codes: ['U','SI','MI','I','VI'],
			scores: [1,2,3,4,5]
		}
	},
	{
		name: 'interLearn',
		condition: 'both',
		group: 'background',
		order: 0,
		battery: ['measureA','measureB'],
		category: ['interest','learning'],
		primary: {
			type: 'likert',
			question: 'Please rate how interested you are about the domain of learning.',
			responses: [
				'Uninterested',
				'Slightly Interested',
				'Moderately Interested',
				'Interested',
				'Very Interested'
			],
			codes: ['U','SI','MI','I','VI'],
			scores: [1,2,3,4,5]
		}
	}
];



// Decisions
var decisionSelected = [
	{
		name: 'con-S1',
		condition: 'both',
		group: 'connection',
		order: 1.1,
		battery: ['decision'],
		category: ['selection','initial','selected'],
		primary: {
			type: 'connect',
			question: 'Please SELECT the panelist you would like on your discussion panel.',
			responses: [
				'High Critical',
				'Medium Neutral',
				'Low Praise'
			],
			codes: ['CRT','NEU','PRA'],
			scores: [1,2,3]
		}
	},
	{
		name: 'con-S2',
		condition: 'struggle',
		group: 'connection',
		order: 4.1,
		battery: ['decision'],
		category: ['selection','final','selected'],
		primary: {
			type: 'connect',
			question: 'Please SELECT the panelist you would like on your discussion panel.',
			responses: [
				'High Critical',
				'Medium Neutral',
				'Low Praise'
			],
			codes: ['CRT','NEU','PRA'],
			scores: [1,2,3]
		}
	},
	{
		name: 'conInfo-S1',
		condition: 'both',
		group: 'connection',
		order: 2,
		battery: ['decision'],
		category: ['importance','initial','selected'],
		primary: {
			type: 'multichoice',
			question: 'What information was most important to your decision of selecting the following panelist?',
			responses: [
				'Personal Info',
				'Knowledge Info',
				'Conversational Info'
			],
			codes: ['P','K','C'],
			scores: [1,2,3]
		}
	},
	{
		name: 'conInfo-S2',
		condition: 'struggle',
		group: 'connection',
		order: 5,
		battery: ['decision'],
		category: ['importance','final','selected'],
		primary: {
			type: 'multichoice',
			question: 'What information was most important to your decision of selecting the following panelist?',
			responses: [
				'Personal Info',
				'Knowledge Info',
				'Conversational Info',
				'Struggle Info'
			],
			codes: ['P','K','C','S'],
			scores: [1,2,3,4]
		}
	},
	{
		name: 'conChange-S',
		condition: 'struggle',
		group: 'connection',
		order: 7,
		battery: ['decision'],
		category: ['change','selected'],
		primary: {
			type: 'open',
			question: 'Why did you @change your selection?',
			responses: [],
			codes: [],
			scores: []
		}
	}
];

var decisionBlocked = [
	{
		name: 'con-B1',
		condition: 'both',
		group: 'connection',
		order: 1.2,
		battery: ['decision'],
		category: ['selection','initial','blocked'],
		primary: {
			type: 'connect',
			question: 'Please BLOCK the panelist you would like on your discussion panel.',
			responses: [
				'High Critical',
				'Medium Neutral',
				'Low Praise'
			],
			codes: ['CRT','NEU','PRA'],
			scores: [1,2,3]
		}
	},
	{
		name: 'con-B2',
		condition: 'struggle',
		group: 'connection',
		order: 4.2,
		battery: ['decision'],
		category: ['selection','final','blocked'],
		primary: {
			type: 'connect',
			question: 'Please BLOCK the panelist you would like on your discussion panel.',
			responses: [
				'High Critical',
				'Medium Neutral',
				'Low Praise'
			],
			codes: ['CRT','NEU','PRA'],
			scores: [1,2,3]
		}
	},
	{
		name: 'conInfo-B1',
		condition: 'both',
		group: 'connection',
		order: 3,
		battery: ['decision'],
		category: ['importance','blocked'],
		primary: {
			type: 'multichoice',
			question: 'What information was most important to your decision of blocking the following panelist?',
			responses: [
				'Personal Info',
				'Knowledge Info',
				'Conversational Info'
			],
			codes: ['P','K','C'],
			scores: [1,2,3]
		}
	},
	{
		name: 'conInfo-B2',
		condition: 'struggle',
		group: 'connection',
		order: 6,
		battery: ['decision'],
		category: ['importance','final','blocked'],
		primary: {
			type: 'multichoice',
			question: 'What information was most important to your decision of blocking the following panelist?',
			responses: [
				'Personal Info',
				'Knowledge Info',
				'Conversational Info',
				'Struggle Info'
			],
			codes: ['P','K','C','S'],
			scores: [1,2,3,4]
		}
	},
	{
		name: 'conChange-B',
		condition: 'struggle',
		group: 'connection',
		order: 8,
		battery: ['decision'],
		category: ['change','blocked'],
		primary: {
			type: 'open',
			question: 'Why did you @change who you blocked?',
			responses: [],
			codes: [],
			scores: []
		}
	}
];



// Measure B
var uncertaintySelected = [
	// Relational Outcomes
	{
		name: 'urtOutTask-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['outcome','task','selected'],
		primary: {
			type: 'likert',
			question: 'Discussion with the panelist __________ improve the quality of my pitch.',
			responses: [
				'Almost certainly\nwill NOT',
				'Probably\nwill NOT',
				'Unsure',
				'Probably\nWILL',
				'Almost certainly\nWILL'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtOutInt-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['outcome','intelligence','selected'],
		primary: {
			type: 'likert',
			question: 'Discussion with the panelist __________ make me become smarter.',
			responses: [
				'Almost certainly\nwill NOT',
				'Probably\nwill NOT',
				'Unsure',
				'Probably\nWILL',
				'Almost certainly\nWILL'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtOutPer-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['outcome', 'personality','selected'],
		primary: {
			type: 'likert',
			question: 'Discussion with the panelist __________ strengthen my personality.',
			responses: [
				'Almost certainly\nwill NOT',
				'Probably\nwill NOT',
				'Unsure',
				'Probably\nWILL',
				'Almost certainly\nWILL'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},

	// Panelist Attitudes
	{
		name: 'urtAttTask-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['attitude','task','selected'],
		primary: {
			type: 'likert',
			question: 'Overall, the panelist will __________ attitude towards my pitch.',
			responses: [
				'Almost certainly\nhave a negative',
				'Probably\nhave a negative',
				'Unsure',
				'Probably\nhave a positive',
				'Almost certainly\nhave a positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtAttInt-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['attitude','intelligence','selected'],
		primary: {
			type: 'likert',
			question: 'Overall, the panelist will __________ attitude towards my intelligence.',
			responses: [
				'Almost certainly\nhave a negative',
				'Probably\nhave a negative',
				'Unsure',
				'Probably\nhave a positive',
				'Almost certainly\nhave a positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtAttPer-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['attitude','personality','selected'],
		primary: {
			type: 'likert',
			question: 'Overall, the panelist will __________ attitude towards my personality.',
			responses: [
				'Almost certainly\nhave a negative',
				'Probably\nhave a negative',
				'Unsure',
				'Probably\nhave a positive',
				'Almost certainly\nhave a positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},


	// Panelist Behaviors
	{
		name: 'urtBehTask-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['behavior','task','selected'],
		primary: {
			type: 'likert',
			question: 'The panelist will __________ things to say about my pitch.',
			responses: [
				'Almost certainly\nhave mostly negative',
				'Probably\nhave mostly negative',
				'Unsure',
				'Probably\nhave mostly positive',
				'Almost certainly\nhave mostly positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtBehInt-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['behavior','intelligence','selected'],
		primary: {
			type: 'likert',
			question: 'The panelist will __________ things to say about my intelligence.',
			responses: [
				'Almost certainly\nhave mostly negative',
				'Probably\nhave mostly negative',
				'Unsure',
				'Probably\nhave mostly positive',
				'Almost certainly\nhave mostly positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtBehPer-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['behavior','personality','selected'],
		primary: {
			type: 'likert',
			question: 'The panelist will __________ things to say about my personality.',
			responses: [
				'Almost certainly\nhave mostly negative',
				'Probably\nhave mostly negative',
				'Unsure',
				'Probably\nhave mostly positive',
				'Almost certainly\nhave mostly positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},

	// Panelist Rejection Prediction
	{
		name: 'urtReject-S',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['rejection', 'selected'],
		primary: {
			type: 'likert',
			question: 'The panelist will __________ my connection request.',
			responses: [
				'Almost certainly\nreject',
				'Probably\nreject',
				'Unsure',
				'Probably\naccept',
				'Almost certainly\naccept'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	}
];

var uncertaintyBlocked = [
	// Relational Outcomes
	{
		name: 'urtOutTask-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['outcome','task','blocked'],
		primary: {
			type: 'likert',
			question: 'Discussion with the panelist __________ have improved the quality of my pitch.',
			responses: [
				'Almost certainly\nwill NOT',
				'Probably\nwill NOT',
				'Unsure',
				'Probably\nWILL',
				'Almost certainly\nWILL'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtOutInt-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['outcome','intelligence','blocked'],
		primary: {
			type: 'likert',
			question: 'Discussion with the panelist __________ have made me become smarter.',
			responses: [
				'Almost certainly\nwill NOT',
				'Probably\nwill NOT',
				'Unsure',
				'Probably\nWILL',
				'Almost certainly\nWILL'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtOutPer-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['outcome','personality','blocked'],
		primary: {
			type: 'likert',
			question: 'Discussion with the panelist __________ have strengthened my personality.',
			responses: [
				'Almost certainly\nwill NOT',
				'Probably\nwill NOT',
				'Unsure',
				'Probably\nWILL',
				'Almost certainly\nWILL'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},

	// Panelist Attitudes
	{
		name: 'urtAttTask-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['attitude','task','blocked'],
		primary: {
			type: 'likert',
			question: 'Overall, the panelist would have __________ attitude towards my pitch.',
			responses: [
				'Almost certainly\nhad a negative',
				'Probably\nhad a negative',
				'Unsure',
				'Probably\nhad a positive',
				'Almost certainly\nhad a positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtAttInt-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['attitude','intelligence','blocked'],
		primary: {
			type: 'likert',
			question: 'Overall, the panelist would have __________ attitude towards my intelligence.',
			responses: [
				'Almost certainly\nhad a negative',
				'Probably\nhad a negative',
				'Unsure',
				'Probably\nhad a positive',
				'Almost certainly\nhad a positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtAttPer-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['attitude','personality','blocked'],
		primary: {
			type: 'likert',
			question: 'Overall, the panelist would have __________ attitude towards my personality.',
			responses: [
				'Almost certainly\nhad a negative',
				'Probably\nhad a negative',
				'Unsure',
				'Probably\nhad a positive',
				'Almost certainly\nhad a positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},

	// Panelist Behaviors
	{
		name: 'urtBehTask-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['behavior','task','blocked'],
		primary: {
			type: 'likert',
			question: 'The panelist would have __________ things to say about my pitch.',
			responses: [
				'Almost certainly\nhad mostly negative',
				'Probably\nhad mostly negative',
				'Unsure',
				'Probably\nhad mostly positive',
				'Almost certainly\nhad mostly positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtBehInt-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['behavior','intelligence','blocked'],
		primary: {
			type: 'likert',
			question: 'The panelist would have __________ things to say about my intelligence.',
			responses: [
				'Almost certainly\nhad mostly negative',
				'Probably\nhad mostly negative',
				'Unsure',
				'Probably\nhad mostly positive',
				'Almost certainly\nhad mostly positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'urtBehPer-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['behavior','personality','blocked'],
		primary: {
			type: 'likert',
			question: 'The panelist would have __________ things to say about my personality.',
			responses: [
				'Almost certainly\nhad mostly negative',
				'Probably\nhad mostly negative',
				'Unsure',
				'Probably\nhad mostly positive',
				'Almost certainly\nhad mostly positive'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},

	// Panelist Rejection Prediction
	{
		name: 'urtReject-B',
		condition: 'both',
		group: 'uncertainty',
		order: 0,
		battery: ['measureB'],
		category: ['rejection','blocked'],
		primary: {
			type: 'likert',
			question: 'The panelist would have __________ my connection request.',
			responses: [
				'Almost certainly\nrejected',
				'Probably\nrejected',
				'Unsure',
				'Probably\naccepted',
				'Almost certainly\naccepted'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	}
];

var perceptionSelected = [

	// For each information section, please rate how positively or negatively you perceived the information provided by the panelist you SELECTED.
	{
		name: 'infoVPer-S',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['valence','personal','selected'],
		primary: {
			type: 'likert',
			question: 'Please rate how positively or negatively you perceived the <strong>Profile: Personal Information</strong> provided by the panelist you SELECTED.',
			responses: [
				'Very Negatively',
				'Negatively',
				'Neutral',
				'Positively',
				'Very Positively'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'infoVKnow-S',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['valence','knowledge','selected'],
		primary: {
			type: 'likert',
			question: 'Please rate how positively or negatively you perceived the <strong>Profile: Knowledge Information</strong> provided by the panelist you SELECTED.',
			responses: [
				'Very Negatively',
				'Negatively',
				'Neutral',
				'Positively',
				'Very Positively'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'infoVStr-S',
		condition: 'struggle',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['valence','struggle','selected'],
		primary: {
			type: 'likert',
			question: 'Please rate how positively or negatively you perceived the <strong>Struggle Information</strong> provided by the panelist you SELECTED.',
			responses: [
				'Very Negatively',
				'Negatively',
				'Neutral',
				'Positively',
				'Very Positively'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'infoVConv-S',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['valence','conversation','selected'],
		primary: {
			type: 'likert',
			question: 'Please rate how positively or negatively you perceived the <strong>Conversational Comments</strong> provided by the panelist you SELECTED.',
			responses: [
				'Very Negatively',
				'Negatively',
				'Neutral',
				'Positively',
				'Very Positively'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},

	// For each information section, please rate how relevant the information was in your decision to SELECT this panelist.
	{
		name: 'infoRPer-S',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['relevance','personal','selected'],
		primary: {
			type: 'likert',
			question: 'Please rate how relevant the <strong>Profile: Personal Information</strong> was in your decision to SELECT this panelist.',
			responses: [
				'Irrelevant',
				'Slightly Relevant',
				'Moderately Relevant',
				'Relevant',
				'Very Relevant'
			],
			codes: ['I','SR','MR','R','VR'],
			scores: [1, 2, 3, 4, 5]
		}
	},
	{
		name: 'infoRKnow-S',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['relevance','knowledge','selected'],
		primary: {
			type: 'likert',
			question: 'Please rate how relevant the <strong>Profile: Knowledge Information</strong> was in your decision to SELECT this panelist.',
			responses: [
				'Irrelevant',
				'Slightly Relevant',
				'Moderately Relevant',
				'Relevant',
				'Very Relevant'
			],
			codes: ['I','SR','MR','R','VR'],
			scores: [1, 2, 3, 4, 5]
		}
	},
	{
		name: 'infoRStr-S',
		condition: 'struggle',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['relevance','struggle','selected'],
		primary: {
			type: 'likert',
			question: 'Please rate how relevant the <strong>Struggle Information</strong> was in your decision to SELECT this panelist.',
			responses: [
				'Irrelevant',
				'Slightly Relevant',
				'Moderately Relevant',
				'Relevant',
				'Very Relevant'
			],
			codes: ['I','SR','MR','R','VR'],
			scores: [1, 2, 3, 4, 5]
		}
	},
	{
		name: 'infoRConv-S',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['relevance','conversation','selected'],
		primary: {
			type: 'likert',
			question: 'Please rate how relevant the <strong>Conversational Comments</strong> were in your decision to SELECT this panelist.',
			responses: [
				'Irrelevant',
				'Slightly Relevant',
				'Moderately Relevant',
				'Relevant',
				'Very Relevant'
			],
			codes: ['I','SR','MR','R','VR'],
			scores: [1, 2, 3, 4, 5]
		}
	}
];

var perceptionBlocked = [

	// For each information section, please rate how positively or negatively you perceived the information provided by the panelist you BLOCKED.
	{
		name: 'infoVPer-B',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['valence','personal','blocked'],
		primary: {
			type: 'likert',
			question: 'Please rate how positively or negatively you perceived the <strong>Profile: Personal Information</strong> provided by the panelist you BLOCKED.',
			responses: [
				'Very Negatively',
				'Negatively',
				'Neutral',
				'Positively',
				'Very Positively'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'infoVKnow-B',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['valence','knowledge','blocked'],
		primary: {
			type: 'likert',
			question: 'Please rate how positively or negatively you perceived the <strong>Profile: Knowledge Information</strong> provided by the panelist you BLOCKED.',
			responses: [
				'Very Negatively',
				'Negatively',
				'Neutral',
				'Positively',
				'Very Positively'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'infoVStr-B',
		condition: 'struggle',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['valence','struggle','blocked'],
		primary: {
			type: 'likert',
			question: 'Please rate how positively or negatively you perceived the <strong>Struggle Information</strong> provided by the panelist you BLOCKED.',
			responses: [
				'Very Negatively',
				'Negatively',
				'Neutral',
				'Positively',
				'Very Positively'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},
	{
		name: 'infoVConv-B',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['valence','conversation','blocked'],
		primary: {
			type: 'likert',
			question: 'Please rate how positively or negatively you perceived the <strong>Conversational Comments</strong> provided by the panelist you BLOCKED.',
			responses: [
				'Very Negatively',
				'Negatively',
				'Neutral',
				'Positively',
				'Very Positively'
			],
			codes: ['VN','N','U','P','VP'],
			scores: [-2, -1, 0, 1, 2]
		}
	},

	// For each information section, please rate how relevant the information was in your decision to BLOCK this panelist.
	{
		name: 'infoRPer-B',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['relevance','personal','blocked'],
		primary: {
			type: 'likert',
			question: 'Please rate how relevant the <strong>Profile: Personal Information</strong> was in your decision to BLOCK this panelist.',
			responses: [
				'Irrelevant',
				'Slightly Relevant',
				'Moderately Relevant',
				'Relevant',
				'Very Relevant'
			],
			codes: ['I','SR','MR','R','VR'],
			scores: [1, 2, 3, 4, 5]
		}
	},
	{
		name: 'infoRKnow-B',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['relevance','knowledge','blocked'],
		primary: {
			type: 'likert',
			question: 'Please rate how relevant the <strong>Profile: Knowledge Information</strong> was in your decision to BLOCK this panelist.',
			responses: [
				'Irrelevant',
				'Slightly Relevant',
				'Moderately Relevant',
				'Relevant',
				'Very Relevant'
			],
			codes: ['I','SR','MR','R','VR'],
			scores: [1, 2, 3, 4, 5]
		}
	},
	{
		name: 'infoRStr-B',
		condition: 'struggle',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['relevance','struggle','blocked'],
		primary: {
			type: 'likert',
			question: 'Please rate how relevant the <strong>Struggle Information</strong> was in your decision to BLOCK this panelist.',
			responses: [
				'Irrelevant',
				'Slightly Relevant',
				'Moderately Relevant',
				'Relevant',
				'Very Relevant'
			],
			codes: ['I','SR','MR','R','VR'],
			scores: [1, 2, 3, 4, 5]
		}
	},
	{
		name: 'infoRConv-B',
		condition: 'both',
		group: 'information',
		order: 0,
		battery: ['measureB'],
		category: ['relevance','conversation','blocked'],
		primary: {
			type: 'likert',
			question: 'Please rate how relevant the <strong>Conversational Comments</strong> were in your decision to BLOCK this panelist.',
			responses: [
				'Irrelevant',
				'Slightly Relevant',
				'Moderately Relevant',
				'Relevant',
				'Very Relevant'
			],
			codes: ['I','SR','MR','R','VR'],
			scores: [1, 2, 3, 4, 5]
		}
	}
];

var rejection = [
	{
		name: 'rejection',
		condition: 'both',
		group: 'rejection',
		order: 1,
		battery: ['rejection'],
		category: [],
		primary: {
			type: 'multichoice',
			question: 'Please select one option before proceeding to your conversation.',
			responses: [
				'Edit my profile / pitch. Resubmit my request to @SELECTED',
				'Use my profile / pitch as is. Try to connect with an alternate panelist.',
				'Do not try to connect with anyone else. Quit the study without my conversation.'
			],
			codes: ['EFFORT','CONT','QUIT'],
			scores: [1,2,3]
		}
	}
];

// Setup for SaveAll
questions = [];
questions = questions.concat(demographic, theoryIntelligence, theoryPersonality, confidenceIntelligence, confidencePersonality, goalOrientation, background, decisionSelected, decisionBlocked, uncertaintySelected, uncertaintyBlocked, perceptionSelected, perceptionBlocked, rejection);
var Question = Parse.Object.extend('Question');
_.each(questions, function(q, index, list){
  var question = new Question(q);
  list[index] = question;
});
// Add ACL
_.each(questions, function(q, index, list){
	var acl = new Parse.ACL();
	acl.setPublicReadAccess(true);
	acl.setRoleWriteAccess('admin', true);
	q.setACL(acl);
});

// Parse.Object.saveAll(questions);


function saveQuestions(questionAry) {
	var Question = Parse.Object.extend('Question');
	var questions = [];

	for (var i = 0; i < questionAry.length; i++) {
		var question = new Question(questionAry[i]);
		var acl = new Parse.ACL();
				acl.setPublicReadAccess(true);
				acl.setPublicWriteAccess(false);
				acl.setRoleWriteAccess('admin', true);
		question.setACL(acl);

		questions.push(question);
	}

	return Parse.Object.saveAll(questions);
}
function setQuestionACL(){
	var query = new Parse.Query('Question');
			query.limit(100);

	query.find().then(function(results) {
		_.each(results, function(question) {
			var acl = new Parse.ACL();
					acl.setPublicReadAccess(true);
					acl.setPublicWriteAccess(false);
					acl.setRoleWriteAccess('admin', true);
			question.setACL(acl);
		});
		Parse.Object.saveAll(results);
	});
}

x = [];
x = x.concat(demographic, theoryIntelligence, theoryPersonality, confidenceIntelligence, confidencePersonality, goalOrientation, background, decisionSelected, decisionBlocked, uncertaintySelected, uncertaintyBlocked, perceptionSelected, perceptionBlocked);
_.each(x, function(q, index, list){
  var question = new Question(q);
  list[index] = question;
});


/*

Algorithm for Confidence Scoring

Very Confident			= abs(3 + 3) = 6
Confident						= abs(3 + 2) = 5
Sort of Confident		= abs(3 + 1) = 4
Sort of Not 				= abs(-4 + 1) = 3
Not Confident				= abs(-4 + 2) = 2
Very Not Confident	= abs(-4 + 3) = 1


codes: ['NOT','CONF'],
scores: [-4, 3]

codes: ['VT','T','ST'],
scores: [3, 2, 1]

Very Not Confident	= abs(-4 + 3) = 1
Not Confident				= abs(-4 + 2) = 2
Sort of Not					= abs(-4 + 1) = 3
Sort of Confident 	= abs(3 + 1) = 4
Confident						= abs(3 + 2) = 5
Very Confident			= abs(3 + 3) = 6

var query = new Parse.Query('Question');
query.equalTo('primary.codes', ['CONF','NOT']);
query.find().then(function(results) {
	console.log(results);
});

*/

// Convert (fix) Confidence values
var query = new Parse.Query('Question');
query.containedIn('group', ['confidenceIntelligence','confidencePersonality']);

query.find().then(function(results) {
	_.each(results, function(question) {
		var primary = question.get('primary');
		var secondary = question.get('secondary');

		if (primary.codes[0] === 'CONF' && primary.codes[1] === 'NOT') {
			console.log([3,-4]);
			primary.scores = [3,-4];
		} else {
			console.log([-4,3]);
			primary.scores = [-4,3];
		}
		secondary.scores = [3,2,1];

		question.set({
			primary: primary,
			secondary: secondary
		});
	});

	Parse.Object.saveAll(results);
});

// Convert (fix) Information Relevance values
var query = new Parse.Query('Question');
query.containedIn('group', ['information']);
query.containedIn('primary.responses', ['Irrelevant']);

query.find().then(function(results) {
	_.each(results, function(question) {
  	var codes = question.get('primary').codes = ['I','SR','MR','R','VR'];
  	var scores = question.get('primary').scores = [1, 2, 3, 4, 5];
	});
	Parse.Object.saveAll(results);
});
