var posts;
var pQuery = new Parse.Query('Post');
		pQuery.limit(500);

pQuery.find().then(function(results){
  posts = results;
});

_.each(posts, function(post){
  post.getACL().setPublicReadAccess(true);
  post.getACL().setPublicWriteAccess(false);
});

Parse.Object.saveAll(posts);