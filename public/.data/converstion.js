// Update Log ACLs
var query = new Parse.Query('Log');
query.limit(1000);
query.find().then(function(logs){
  _.each(logs, function(log, index, list){
    var acl = new Parse.ACL(log.get('user'));
    acl.setRoleReadAccess('admin', true);
    acl.setRoleWriteAccess('admin', true);
    log.setACL(acl);
  });

  return Parse.Object.saveAll(logs);
}).then(function(logs){
  window.xlogs = logs;
});