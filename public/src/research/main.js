require.config({
	baseUrl: '/lib',
	paths: {
		// Core Paths
		backbone: 'backbone',
		bootstrap: 'bootstrap',
		has: 'has',
		jquery: 'jquery',
		moment: 'moment',
		parseSDK: 'parse-1.4.2',
		parse: '../src/research/utilities/ParseReady',
		underscore: 'underscore',

		// Helper Paths
		models: '../src/research/models',
		views: '../src/research/views',
		collections: '../src/research/collections',
		utils: '../src/research/utilities',
		tmp: '../templates/research'
	},
	shim: {
		bootstrap: {
			deps: ['jquery'],
			exports: 'jquery'
		},
		parseSDK: {
			exports: 'Parse'
		}
	},
	config: {
		"parse": {
			// For App -> yurt-test
			// appKey: "z5yKy3MFzauGU7YpVu8952LYMWBhV8JUdSf9galQ",
			// jsKey: "BKIyequuCjBhZ6aFRCpLfRv5NHNNvVxg4I8uJI96"
			// For App -> yurt (production)
			appKey: "k7rXCYqMed5AViXrMYbCiMtlXTSN3AqiIX8WL9Km",
			jsKey: "QNuqeYZnFlqLnl9vCJvLFFCNSNNqlZWP7HZpHWvU"
			// For App -> yurt_mee
			// appKey: "isrcMtWYbVc6pA5wos1DGTEGWTVktelQReq38hLZ",
			// jsKey: "eZOHfmLG083pngEhON95N8CYP6TDQ0wSSwoKdyVN",
		}
	}
});

require([
	'parse',
	'utils/Setup',
	'utils/Router',
	'views/App',
	'views/Navbar',
	'views/Modal',
	'utils/Analytics'
], function(Parse, Setup, Router, AppView, Navbar, Modal, Analytics) {

	// Setup Yurt object and load any app/namespace properties
	Setup();

	if (yurt.mobileCheck()) {
		window.location.href = '/mobile';
	} else {

		// Wait for User to become() before starting app
		Backbone.listenToOnce(Backbone, 'userAvailable', function() {
			var appView = new AppView({
				el: '#app'
			});
			var navbarView = new Navbar({
				el: '#navbar'
			});
			appView.subviews = [navbarView];

			yurt.router = new Router({
				app: appView
			});
			yurt.app = appView;

			var modal = new Modal({
				el: '#modal'
			});
			appView.modal = modal;
			
			appView.render();
			navbarView.render();

			// Start routing
			Backbone.history.start({pushState: true});	
		});

		// Always BECOME
		Parse.User.become($('[name="sessionToken"]').attr('content')).then(function(user) {
			yurt.user = user;

			// Setup Analytics
			yurt.analytics = new Analytics({
				user: yurt.user
			});

			$(window).one('beforeunload', function() {
				yurt.analytics.destroy();
			});

			return yurt.analytics.initialize();

		}).then(function(log) {
			Backbone.trigger('userAvailable');
		});
	}

});