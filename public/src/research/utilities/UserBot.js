define([
	'underscore',
	'backbone'
], function(_, Backbone) {
	'use strict';

	var UserBot = function() {
		
		this.MAXUSERS = 21;		// Absolute maximum number of users
		this.MAXCHANGES = 60;	// Maximum number of user count changes per hour
		
		// 24 hour clock indices and percentage of general traffic
		// PEAK = 20th hour, LOW = 3rd hour
		this.userDist = [0.45,0.30,0.23,0.18,0.18,0.28,0.46,0.60,0.62,0.63,0.65,0.65,0.67,0.67,0.67,0.72,0.75,0.82,0.90,0.95,1.00,1.00,0.85,0.62];

		this.userCount = this.getExpectedCount();

		// Start user count timer
		this.initialize();

		this.notifySystem(this.userCount);
		// console.log('UserBot notified navbar with count');

		// var x = setInterval(function() {
		// 	console.count('seconds');
		// }, 1000);
	};

	UserBot.prototype = {
		initialize: function() {
			// console.log('UserBot initialized');
			var that = this;
			var HOUR = 1000 * 60 * 60;
			var date = new Date();

				// date.setHours(3);
				// HOUR = 1000 * 30;

			var currentHour = date.getHours();
			var msRemaining = HOUR - date.getMilliseconds();
			var remainingFraction = msRemaining / HOUR;

			var calcNumUpdates = function() {
				// return Math.ceil(that.MAXCHANGES * that.userDist[currentHour] + (0.5 * (1 - that.userDist[currentHour]) ) * remainingFraction);
				// return that.userDist[currentHour] * that.MAXUSERS * 5;
				return 60;
			};

			// Get number of updates that should happen based on the traffic rate
			// var numUpdates = Math.ceil(that.MAXCHANGES * that.userDist[currentHour] * remainingFraction);
			var numUpdates = calcNumUpdates();

			// Setup the schedule for the hour or remainder of
			var schedule = this.getSchedule(numUpdates, msRemaining);

			this.runUpdates(schedule, function() {

				// Delay to the hour start
				_.delay(function() {
					console.log('Running NEXT hour');

					// numUpdates = Math.ceil(that.MAXCHANGES * that.userDist[currentHour] * remainingFraction);
					numUpdates = calcNumUpdates();
					schedule = that.getSchedule(numUpdates, HOUR);
					that.runUpdates(schedule);

					// msRemaining = HOUR;

					that.timer = setInterval(function() {
						// numUpdates = Math.ceil(that.MAXCHANGES * that.userDist[currentHour] * remainingFraction);
						numUpdates = calcNumUpdates();
						schedule = that.getSchedule(numUpdates, HOUR);
						that.runUpdates(schedule);
					}, HOUR);

				}, msRemaining - _.last(schedule));

			});
		},
		getSchedule: function(numUpdates, timeSpan) {

			var schedule = [];
			_.times(numUpdates, function(index) {
				var time = Math.floor(Math.random() * timeSpan);
				schedule.push(Math.floor(time));
			});

			return _.sortBy(schedule);
		},
		runUpdates: function(schedule, callback) {
			var that = this;

			var index = 0;
			var update = function() {
				console.log('UPDATED');

				// UPDATE CODE HERE
				that.setUserCount();
				that.notifySystem(that.userCount);

				index = index + 1;

				if (index < schedule.length) {
					_.delay(update, schedule[index] - schedule[index-1]);
				} else {
					console.log('NO MORE UPDATES');
					if (callback && typeof callback === 'function') {
						callback();
					}
				}
			};

			_.delay(update, schedule[index]);

		},
		setUserCount: function() {
			var expected = this.getExpectedCount();
			var bias = 0.05; // 5% incremental bias

			if (this.userCount > expected) {
				bias = -1 * bias * (this.userCount - expected);
			} else if (this.userCount < expected) {
				bias = bias * (expected - this.userCount);
			} else {
				bias = 0;
			}

			var rand = Math.random() + bias;
					rand = rand < 0 ? 0 : rand;

			var increment = Math.round(rand) ? 1 : -1;

			this.userCount += increment;

		},
		notifySystem: function(count) {
			Backbone.trigger('system:userCount', count);
		},
		getExpectedCount: function() {

			var date = new Date();


					// date.setHours(3);


			var hour = date.getHours();
			var minutes = date.getMinutes();
			var hourCompleted = (minutes / 60); // Percent (rate) of current hour completed

			var currentRate = this.userDist[hour];
			var nextRate;

					if ((hour + 1) > (this.userDist.length - 1)) {
						nextRate = this.userDist[0];
					} else {
						nextRate = this.userDist[hour + 1];
					}

			var trueRate = (nextRate - currentRate) * hourCompleted + currentRate;
			var expectedUsers = Math.ceil(this.MAXUSERS * trueRate);

			return expectedUsers;
		}
	};

	return UserBot;
});