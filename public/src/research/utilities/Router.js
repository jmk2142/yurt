define([
	'backbone',
	'models/Question',
	'collections/QuestionList',
	'views/Home',
	'views/Survey',
	'views/ProfileMake',
	'views/Social',
	'views/Outcome'
], function(Backbone, Question, QuestionList, HomeView, SurveyView, ProfileMakeView, SocialView, OutcomeView) {

	var Router = Backbone.Router.extend({
		routes: {

			'logout': 'logout',
			'start': 'start',
			'survey/:battery': 'survey',
			'profile': 'profile',
			'social': 'social',
			'social/:question': 'social',
			'outcome': 'outcome',
			'*path': 'default'
		},
		initialize: function(options) {
			this.app = options && options.app ? options.app : undefined;
			// console.log(this.app);
		},
		logout: function() {
			Parse.User.logOut().then(function() {
				window.location.href = "/logout";
			});
		},
		start: function() {
			if (yurt.config.get('progress') === 'start') {
				console.log('START EXPLANATION PAGE');
				var view = new HomeView();
				this.app.showView(view);
			} else {
				this.navigate(yurt.config.get('progress'), {trigger:true});
			}
		},
		survey: function(battery) {
			Backbone.trigger('help:hide');
			var progress = yurt.config.get('progress');
			var measures = [
				'survey/demographic',
				'survey/measureA',
				'survey/decision',
				'survey/measureB'
			];

			if (_.contains(measures, progress)) {

				console.log('SURVEY PAGE', 'MEASURE: ' + battery);

				var questionIDs = yurt.config.get('battery')[battery];
				var questions = _.chain(questionIDs).map(function(id) {
					return Question.createWithoutData(id);
				}).value();

				// Build the survey collection
				var collection = new QuestionList(questions, {
					comparator: function(model) {
						return yurt.config.get('battery')[battery].indexOf(model.id);
					}
				});

				var view = new SurveyView({
					collection: collection,
					battery: battery
				});
				this.app.showView(view);

				// // Build the survey collection
				// var collection = new QuestionList();
				// 		collection.setMode(battery);

				// var view = new SurveyView({
				// 	collection: collection,
				// 	battery: battery
				// });
				// this.app.showView(view);

			} else {
				this.navigate(progress, {trigger:true});
			}
		},
		profile: function() {
			var that = this;

			if (yurt.config.get('progress') === 'profile') {
				// console.log('PROFILE MAKE PAGE');

				// Get their profile
				var query = new Parse.Query('Profile');
						query.equalTo('user', yurt.user);

				var view;

				query.first().then(function(profile) {
					
					if (profile) {
						view = new ProfileMakeView({
							model: profile
						});
					} else {
						view = new ProfileMakeView();
					}

					that.app.showView(view);
					Backbone.trigger('shadow', false);
				});

			} else {
				this.navigate(yurt.config.get('progress'), {trigger:true});
			}
		},
		social: function(questionID) {
			questionID = questionID || undefined;

			this.navigate('social', {replace:true});

			var progress = yurt.config.get('progress');

			if (progress.match(/^social/)) {
				var view = new SocialView({
					progress: questionID
				});
				this.app.showView(view);
				Backbone.trigger('shadow', false);

			} else {
				console.log('BREAK');
				this.navigate(yurt.config.get('progress'), {trigger:true});
			}
		},
		outcome: function() {
			var that = this;

			var progress = yurt.config.get('progress');

			if (progress === 'outcome') {

				var condition = yurt.config.get('condition');
				var qNameS = (condition === 'control') ? 'con-S1' : 'con-S2';
				var qNameB = (condition === 'control') ? 'con-B1' : 'con-B2';

				var selectResQuery = new Parse.Query('Response');
						selectResQuery.equalTo('user', yurt.user);
						selectResQuery.equalTo('name', qNameS);
				var blockResQuery = new Parse.Query('Response');
						blockResQuery.equalTo('user', yurt.user);
						blockResQuery.equalTo('name', qNameB);

				var questionQuery = new Parse.Query('Question');
						questionQuery.containedIn('battery', ['rejection']);

				var p1 = Parse.Query.or(selectResQuery, blockResQuery).find();
				var p2 = questionQuery.first();

				Parse.Promise.when(p1, p2).then(function(responses, rejectionQ) {
					console.log(responses);

					var selectedRes = _.find(responses, function(response) {
						return response.get('name') === qNameS;
					});
					var blockedRes = _.find(responses, function(response) {
						return response.get('name') === qNameB;
					});

					var view = new OutcomeView({
						model: rejectionQ,
						battery: 'rejection',
						selectedRes: selectedRes,
						blockedRes: blockedRes
					});
					that.app.showView(view);
				});

			} else {
				this.navigate(progress, {trigger:true});
			}
		},
		default: function() {
			var progress = yurt.config.get('progress');
			// console.log('DEFAULT');
			this.navigate(progress, {trigger:true});
		}
	});

	return Router;

});
