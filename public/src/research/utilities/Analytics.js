define([
	'jquery',
	'underscore',
	'parse',
	'utils/Timer'
], function($, _, Parse, Timer) {
	'use strict';
	
	var Log = Parse.Object.extend('Log');

	var Analytics = function(options) {
		options = options || {};
		this.user = options.user || yurt.user || undefined;

		this.timer = new Timer();
	};

	Analytics.prototype = {
		initialize: function() {
			var initialLog = new Log({
				user: this.user,
				name: 'session',
				section: null,
				context: ['initialized'],
				character: null,
				duration: null,
				idleTime: null,
				activeTime: null,
				lengthInitial: null,
				lengthChange: null,
				lengthFinal: null
			});

			var acl = new Parse.ACL(yurt.user);
					acl.setRoleReadAccess('admin', true);
					acl.setRoleWriteAccess('admin', true);

			initialLog.setACL(acl);

			return initialLog.save();
		},
		terminate: function() {
			var terminalLog = new Log({
				user: this.user,
				name: 'session',
				section: null,
				context: ['terminated'],
				character: null,
				duration: null,
				idleTime: null,
				activeTime: null,
				lengthInitial: null,
				lengthChange: null,
				lengthFinal: null
			});

			var acl = new Parse.ACL(yurt.user);
					acl.setRoleReadAccess('admin', true);
					acl.setRoleWriteAccess('admin', true);
					
			terminalLog.setACL(acl);

			return terminalLog.save();
		},
		track: function(name, dimensions) {
			dimensions = dimensions || {};

			// Clear the previous log
			if (this.currentLog) {
				this.clear();
			}
	
			// If watching a specific el (for blur)...
			if (dimensions.watchEl) {
				// Tranform watchEl into a jQuery object
				this.watchEl = dimensions.watchEl instanceof jQuery ? dimensions.watchEl : $(dimensions.watchEl);
			} else {
				this.watchEl = null;
			}

			// Gotta figure out the standardization of dimensions
			var section = dimensions.section ? dimensions.section : null;
			// Change section name
			switch (section) {
				case 'self-pitch': section = 'pitch'; break;
				case 'self-personal': section = 'personal'; break;
				case 'self-knowledge': section = 'knowledge'; break;
				default: section = section;
			}

			var context = dimensions.context && _.isArray(dimensions.context) ? dimensions.context : null;
			var character = dimensions.character ? dimensions.character : null;
			var lengthInitial = this.watchEl ? this.watchEl.val().length : null;

			this.currentLog = new Log({
				user: this.user,
				name: name,
				section: section,
				context: context,
				character: character,
				duration: 0,
				idleTime: 0,
				activeTime: 0,
				lengthInitial: lengthInitial,
				lengthChange: null,
				lengthFinal: null
			});

			this.timer.start(section);

			var acl = new Parse.ACL(yurt.user);
					acl.setRoleReadAccess('admin', true);
					acl.setRoleWriteAccess('admin', true);
					
			this.currentLog.setACL(acl);

			this.currentLog.save();

		},
		clear: function(options) {
			options = options || {};

			var currentSection = this.currentLog.get('section');
			var duration = this.timer.getTime(currentSection);
			var idleTime = this.timer.getIdleTime(currentSection);

			this.currentLog.save({
				duration: duration,
				idleTime: idleTime,
				activeTime: duration - idleTime,
				lengthFinal: this.watchEl ? this.watchEl.val().length : null,
				lengthChange: this.watchEl ? this.watchEl.val().length - this.currentLog.get('lengthInitial') : null
			});

			this.currentLog = null;
			this.watchEl = null;

			this.timer.remove(currentSection);
		},
		destroy: function(options) {
			this.clear(options);

			this.terminate();

			this.timer.destroy();
		}
	};

	return Analytics;

});