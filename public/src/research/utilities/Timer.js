// idleTime method: http://stackoverflow.com/questions/667555/detecting-idle-time-in-javascript-elegantly

define([
	'underscore'
], function(_) {
	'use strict';

	var Timer = function() {

		var that = this;
		
		this.interval = 1000;	// 1 second
		this.idleTime = 0;
		this.idle = false;

		function timerIncrement() {
			
			// idleStart in seconds
			var minutes = 5; // in minutes
			var IDLEAFTER = minutes * 60;	// minutes x 60 seconds

			that.idleTime++;

			if (that.idleTime > (IDLEAFTER - 1)) {
				// console.log('idle:' + that.idleTime + ' | minutes: ' + that.idleTime/60);
				// console.log(that.idleTime, that.idleTime / 60);
				that.idle = true;
			} else {
				// console.log(that.idleTime);
				that.idle = false;
			}
		}

		this.idleTimer = setInterval(timerIncrement, this.interval);

		this.timers = {
			'default': {
				time: 0,
				idleTime: 0,
				idle: false,
				active: false
			}
		};

		// Set mouse and key listeners
		$(window).mousemove(function(event) {
			that.idleTime = 0;
		});
		$(window).keypress(function(event) {
			that.idleTime = 0;
		});
		$(window).bind('mousewheel', function(event){
			that.idleTime = 0;
		});
	};

	Timer.prototype = {
		_stop: function() {
			clearInterval(this.idleTimer);
		},
		_reset: function() {
			this.idleTime = 0;
			this.idle = false;
		},
		_destroy: function() {
			this._stop();
			this._reset();

			// Kill the event listeners
			$(window).off('mousemove');
			$(window).off('keypress');
			$(window).off('mousewheel');
		},
		getTime: function(label) {
			var time;

			if (label) {
				time = this.timers[label].time;
			} else {
				time = {};

				for (var key in this.timers) {
					time[key] = this.timers[key].time;	
				}
			}

			return time;
		},
		getIdleTime: function(label) {
			var idleTime;

			if (label) {
				idleTime = this.timers[label].idleTime;
			} else {
				idleTime = {};

				for (var key in this.timers) {
					idleTime[key] = this.timers[key].idleTime;	
				}
			}

			return idleTime;
		},
		start: function(label) {
			var that = this;

			label = label ? label : 'default';

			// No Specific Timer
			if (!this.timers[label]) {
				this.timers[label] = {
					time: 0,
					idleTime: 0,
					idle: false,
					active: false
				};
			}

			// Only start a timer if it is inactive
			if (!this.timers[label].active) {

				var timer = setInterval(function(){
					that.timers[label].time++;

					if (that.idle) {	// Timer() idle
						if (that.timers[label].idle) {	// specific active
							// old idle so increment it by 1
							that.timers[label].idleTime++;
						} else {	// specific idle
							// new idle so increment it by idle time
							that.timers[label].idleTime += that.idleTime;
							that.timers[label].idle = true;
						}
					} else {	// Timer() active
						// reset specific idle
						that.timers[label].idle = false;
					}

				}, 1000);

				// Starts the TIMER
				this.timers[label].timer = timer;
				this.timers[label].active = true;
			}

			return this;
		},
		startOne: function(label) {
			this.stop();
			this.start(label);
		},
		stop: function(label) {
			if (label) {
				clearInterval(this.timers[label].timer);
				this.timers[label].active = false;
			} else {
				for (var key in this.timers) {
					clearInterval(this.timers[key].timer);
					this.timers[key].active = false;
				}
			}
			return this.getTime();
		},
		reset: function(label) {
			if (label) {
				this.stop(label);
				this.timers[label].time = 0;
			} else {
				for (var key in this.timers) {
					this.stop(key);
					this.timers[key].time = 0;
				}
			}
			return this.getTime();
		},
		remove: function(label) {
			if (label) {
				this.stop(label);
				this.reset(label);
				delete this.timers[label];
			} else {
				for (var key in this.timers) {
					this.stop(label);
					this.reset(label);

					// Save default
					if (key !== 'default') {
						delete this.timers[key];
					}
				}
			}
		},
		// Destroys all the object things / unbinding etc.
		destroy: function() {
			this.remove();
			this._destroy();
		}
	};

	return Timer;

});

