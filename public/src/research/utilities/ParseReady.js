define([
	'parseSDK',
	'backbone',
	'module'
], function(Parse, Backbone, module) {

	// Initialize Parse
	Parse.initialize(module.config().appKey, module.config().jsKey);
	
	// Use updated Backbone.Events for Parse
	Parse.Events = Backbone.Events;
	_.extend(Parse.Collection.prototype, Backbone.Events);
	_.extend(Parse.Object.prototype, Backbone.Events);

	return Parse;
});