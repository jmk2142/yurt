define([
	'jquery',
	'underscore',
	'backbone',
	'text!tmp/profile-editor.ejs'
], function($, _, Backbone, ProfileEditorTMP) {
	'use strict';
	
	var Profile = Backbone.View.extend({
		id: 'profileEditor',
		className: 'profileEditor',
		template: _.template(ProfileEditorTMP),
		events: {
			'click #self-profileTabs [data-section]': 'clickSection',
			'focus textarea, input': 'track',
			'blur textarea, input': 'saveProfile'
		},
		track: function(event) {
			event = event || {};
			var $target = $(event.currentTarget);
			var context = $target.data('context');
			if (context) {
				context = context.split(',');
			} else {
				context = this.firstProfile ? ['initial'] : ['social'];
			}

			var eventType = event.type;
			
			
			if (eventType === 'focusin') {
				// var context = $target.data('context');

				yurt.analytics.track('edit', {
					section: this.section,
					character: 'SELF',
					context: context,
					watchEl: $target
				});

			// VIEW event
			} else {
				yurt.analytics.track('view', {
					section: this.section,
					character: 'SELF',
					// context: this.firstProfile ? ['initial'] : undefined
					context: context,
				});
			}
		},
		initialize: function(options) {

			options = options || {};
			this.firstProfile = options.firstProfile;

			this.section = 'self-pitch';

			if (!this.model) {
				var Profile = Parse.Object.extend('Profile');
				this.model = new Profile({
					user: yurt.user,
					pitch: '',
					personal: {
						hobby: '',
						attitude: '',
						trait: ''
					},
					knowledge: ''
				});

				var acl = new Parse.ACL(yurt.user);
						acl.setRoleReadAccess('admin', true);
						acl.setRoleWriteAccess('admin', true);
				this.model.setACL(acl);
			}

		},
		render: function() {
			var data = this.model.toJSON();

			var name = yurt.user.get('first') + ' ' + yurt.user.get('last') + "'s";
					name = name.replace(/(s')s|(z')s/g, "$1$2");
			var initials = yurt.user.get('first').toUpperCase()[0] + yurt.user.get('last').toUpperCase()[0];

			data = _.extend(data, {
				initials: initials,
				name: name,
				firstProfile: this.firstProfile
			});

			this.$el.html(this.template(data));

			this.updateProfile();

			this.toggleSection();

			return this;
		},

		// SECTION change and toggle
		clickSection: function(event) {
			var $target, section;
			
			$target = $(event.currentTarget);
			section = $target.data('section');

			this.setSection({
				section: section
			});

			this.track(event);
		},
		setSection: function(options) {
			options = options || {};
			options.section = options.section ? options.section : 'self-pitch';

			// SET CHARCTER
			this.character = 'SELF';

			// SET SECTION
			this.section = options.section;

			this.toggleSection();
		},
		toggleSection: function() {
			this.$('#self-profileTabs [data-section="' + this.section + '"]').tab('show');
		},

		// PROFILE update, set, and save
		updateProfile: function() {
			this.$('#profile-pitch').val(this.model.get('pitch'));
			this.$('#profile-hobby').val(this.model.get('personal').hobby);
			this.$('#profile-attitude').val(this.model.get('personal').attitude);
			this.$('#profile-trait').val(this.model.get('personal').trait);
			this.$('#profile-knowledge').val(this.model.get('knowledge'));
		},
		setProfile: function() {
			this.model.set({
				pitch: this.$('#profile-pitch').val(),
				personal: {
					hobby: this.$('#profile-hobby').val(),
					attitude: this.$('#profile-attitude').val(),
					trait: this.$('#profile-trait').val()
				},
				knowledge: this.$('#profile-knowledge').val()
			});
		},
		saveProfile: function(event) {
			this.setProfile();
			this.model.save().then(function() {
				Backbone.trigger('system:push', {
					icon: 'save',
					message: 'Saved Profile',
					urgent: true
				});
			}, function(error) {
				Backbone.trigger('system:push', {
					icon: 'error',
					message: 'Error: ' + error.message,
					urgent: true
				});
			});
			this.track();
		}
	});

	return Profile;

});