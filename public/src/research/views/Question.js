define([
	'jquery',
	'underscore',
	'backbone',
	'bootstrap',
	'models/Response',
	'text!tmp/question.ejs'
], function($, _, Backbone, Bootstrap, Response, QuestionTMP) {
	'use strict';
	
	var View = Backbone.View.extend({
		className: 'question',
		template: _.template(QuestionTMP),

		// DEFAULT EVENTS
		events: {
			'click input': 'saveResponse'
		},
		// LINKED Q EVENTS
		eventsLinked: {
			'click .primary input': 'showLinked',
			'click .primary input, .secondary input': 'saveResponse'
		},
		// OPEN Q EVENTS
		eventsOpen: {
			'blur textarea': 'saveResponse'
		},
		initialize: function(options) {
			var that = this;
			this.qType = this.model.getType();
			this.parent = options.parent;

			if (this.qType === 'linked') {
				this.delegateEvents(this.eventsLinked);
			} else if (this.qType === 'open') {
				this.delegateEvents(this.eventsOpen);
			} else if (this.qType === 'connect') {
				// Listen to Parent for selection / blocking
				this.listenTo(options.parent, 'selection', this.updateSelection);
			}

			// Process user response if it already exists
			if (options.response) {
				this.response = options.response;
			} else {
				this.response = new Response({
					user: yurt.user,
					question: this.model,
					name: this.model.get('name'),
					group: this.model.get('group'),
					battery: options.battery
				});
				
				var acl = new Parse.ACL(yurt.user);
				acl.setRoleReadAccess('admin', true);
				acl.setRoleWriteAccess('admin', true);
				this.response.setACL(acl);
			}

			// Process reference responses if available
			if (options.reference) {
				this.reference = options.reference;

				var selectionFinal = ['con-S2','con-B2'];
				var responseIsNew = this.response.isNew();

				// Auto-mark reference selection and blocking
				if (responseIsNew && _.contains(selectionFinal, this.model.get('name'))) {
					
					this.response.set({
						response: this.reference.get('response'),
						code: this.reference.get('code'),
						score: this.reference.get('score')
					});

					this.response.save().then(function(response) {
						that.parent.responses.push(response);
						that.parent.render();
					});

				}
			}

			this.listenTo(Backbone, 'questions:toggled', this.refreshTooltip);
		},
		refreshTooltip: function() {
			if (this.tooltip) {
				this.$el.tooltip('destroy');
				this.hintSkipped();
			}
		},
		render: function() {
			var data = this.model.toJSON();
			data = _.extend(data, {
				type: this.qType
			});

			var changeQs = ['conChange-S','conChange-B'];

			if (this.reference && _.contains(changeQs, this.model.get('name'))) {
				console.log('REFERENCE: ', this.reference);
				var different = false;
				if (this.reference[0].get('code') !== this.reference[1].get('code')) {
					// DIFFERENT = CHANGED
					data.primary = _.extend(data.primary, {
						question: data.primary.question.replace(/@change/, 'change')
					});
				} else {
					// NOT DIFFERENT = KEPT
					data.primary = _.extend(data.primary, {
						question: data.primary.question.replace(/@change/, 'not change')
					});
				}
			} else if (this.model.get('group') === 'rejection') {
				
				var selectedChar = this.reference.selected;
				var blockedChar = this.reference.blocked;

				_.each(data.primary.responses, function(response, index, list) {
					list[index] = response.replace(/@SELECTED/, selectedChar.first + ' ' + selectedChar.last);
				});
			}

			this.$el.html(this.template(data));

			this.$el.attr('id', this.model.id);
			var selectionQs = ['con-S1','con-B1','con-S2','con-B2'];
			if (_.contains(selectionQs, this.model.get('name'))) {
				this.$el.attr('class', '');
			}

			// Add user response if it exists
			this.markResponse();

			return this;
		},
		updateSelection: function(data) {
			var action = data.action;
			var thisAction = _.contains(this.model.get('category'), 'selected') ? 'selected' : 'blocked';

			// Updates this response
			if (action === thisAction) {
				// Updates the input value
				this.$('input').val(data.code);
				this.saveResponse();

			// Updates the other response
			} else {

				if (this.response.get('code') === data.code) {
					this.$('input').val(null);
					this.saveResponse();
				}

			}
		},
		// Changes the value of the question input based on response object
		markResponse: function(data) {
			if (!this.response.isNew()) {

				// LINKED Qs
				if (this.qType === 'linked') {
					var codes = this.response.get('code').split('|');
					this.$('.primary input[value="' + codes[0] + '"]').prop('checked', true);
					this.$('.secondary input[value="' + codes[1] + '"]').prop('checked', true);
					this.showLinked();

				// OTHER Qs
				} else if (this.qType === 'open') {
					this.$('textarea').val(this.response.get('response'));
				} else if (this.qType === 'connect') {
					this.$('input').val(this.response.get('code'));
				} else {
					this.$('input[value="' + this.response.get('code') + '"]').prop('checked', true);
				}

			}
		},
		// Sets the value of the response object from input
		setResponse: function(options) {
			var primary = this.model.get('primary');
			var data;
			var codePrimary;

			// Grab the input value
			if (this.qType === 'open') {
				codePrimary = this.$('textarea').val();
			} else if (this.qType === 'connect') {
				codePrimary = this.$('input[type="hidden"]').val();
				console.log(codePrimary);
			} else {
				codePrimary = this.$('.primary input:checked').val();
			}

			// Set the response data
			if (this.qType === 'linked') {
				var secondary = this.model.get('secondary');
				var codeSecondary = this.$('.secondary input:checked').val();

				if (codeSecondary) {
					data = {
						response: primary.responses[primary.codes.indexOf(codePrimary)] + '|' + secondary.responses[secondary.codes.indexOf(codeSecondary)],
						code: codePrimary + '|' + codeSecondary,
						score: this.model.getScore(codePrimary, codeSecondary)
					};
				} else {
					data = {
						response: primary.responses[primary.codes.indexOf(codePrimary)],
						code: codePrimary,
						score: null
					};
				}

			} else if (this.qType === 'open') {
				data = {
					response: codePrimary,
					code: null,
					score: null
				};
			} else {
				data = {
					response: primary.responses[primary.codes.indexOf(codePrimary)] || null,
					code: codePrimary || null,
					score: this.model.getScore(codePrimary) || null
				};
			}

			this.response.set(data);
		},
		checkLast: function() {
			Backbone.trigger('check:previous', this.model.id);
		},
		saveResponse: function() {
			console.log('saveResponse');

			this.setResponse();
			
			this.response.save().then(function(response) {
				// console.log(response.toJSON());
			});
			this.checkLast();
			this.$el.tooltip('destroy');
			this.tooltip = false;

			// Let Survey know that a question was completed
			if (this.response.get('group') !== 'confidence') {
				Backbone.trigger('question:answered', {
					question: this.model,
					response: this.response
				});				
			} else {
				if (this.response.get('score')) {
					Backbone.trigger('question:answered', {
						question: this.model,
						response: this.response
					});
				}
			}

		},
		showLinked: function(event) {
			this.$('.secondary').removeClass('hidden');
		},
		hintSkipped: function() {
			this.$el.tooltip({
				title: 'Did you forget to answer this?',
				trigger: 'manual',
				placement: 'left',
				animation: false,
				container: '#' + this.model.id,
				viewport: '#app'
			});
			this.$el.tooltip('show');
			this.tooltip = true;
		}
	});

	return View;

});