define([
	'jquery',
	'underscore',
	'backbone',
	'text!tmp/home.ejs'
], function($, _, Backbone, HomeTMP) {
	'use strict';
	
	var HomeView = Backbone.View.extend({
		id: 'home',
		className: 'page home',
		template: _.template(HomeTMP),
		events: {
			'click #next': 'gotoNext'
		},
		initialize: function() {
			console.log('HomeView init');

			yurt.analytics.track('view', {
				section: 'introduction',
				character: null,
				context: []
			});
		},
		render: function() {
			console.log('HomeView render');
			var username = yurt.user.getUsername();
			var data = {
				// first: yurt.getLocal(username).first,
				// last: yurt.getLocal(username).last,

				first: yurt.user.get('first'),
				last: yurt.user.get('last'),
				username: username
			};
			this.$el.html(this.template(data));
			return this;
		},
		gotoNext: function() {
			yurt.config.save('progress','survey/demographic');
			yurt.router.navigate('survey/demographic', {trigger:true});
		}
	});

	return HomeView;

});