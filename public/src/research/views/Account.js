define([
	'jquery',
	'underscore',
	'backbone',
	'text!tmp/account.ejs'
], function($, _, Backbone, AccountTMP) {
	'use strict';

	var AccountView = Backbone.View.extend({
		id: 'account',
		className: 'page account',
		template: _.template(AccountTMP),
		events: {},
		initialize: function() {
			console.log('AccountView init');
		},
		render: function(options) {
			this.$el.html(this.template());
			return this;
		}
	});

	return AccountView;

});
