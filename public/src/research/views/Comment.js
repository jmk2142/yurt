define([
	'jquery',
	'underscore',
	'backbone',
	'text!tmp/comment.ejs'
], function($, _, Backbone, CommentTMP) {
	'use strict';
	
	var Comment = Backbone.View.extend({
		className: 'comment clearfix',
		template: _.template(CommentTMP),
		events: {
			'click .fa-remove': 'archive'
		},
		initialize: function(options) {

		},
		render: function() {
			var data = this.model.toJSON();
					data = _.extend(data, {
						author: data.author !== '' ? data.author : 'Anonymous'
					});

			this.$el.html(this.template(data));

			return this;
		},
		archive: function(event) {
			this.model.save({
				status: 'removed'
			}).then(function() {
				Backbone.trigger('system:push', {
					icon: 'delete',
					message: 'Deleted Comment',
					urgent: true
				});
			}, function(error) {
				Backbone.trigger('system:push', {
					icon: 'error',
					message: 'Error: ' + error.message,
					urgent: true
				});
			});
			this.remove();
			this.model.collection.remove(this.model);
		}
	});

	return Comment;
});
