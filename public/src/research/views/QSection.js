define([
	'jquery',
	'underscore',
	'backbone',
	'text!tmp/qSection.ejs'
], function($, _, Backbone, QSectionTMP) {
	'use strict';
	
	var QSection = Backbone.View.extend({
		className: 'qSection',
		template: _.template(QSectionTMP),
		events: {},
		initialize: function(options) {
			this.refResponses = options.refResponses;
		},
		render: function() {
			var data = this.getData();
			this.$el.html(this.template(data));
			return this;
		},
		getData: function() {
			var data = this.model.toJSON();

			data = _.extend(data, this.getReference());

			data = _.extend(data, {
				section: QSection.getSection(this.model)
			});

			return data;
		},
		getReference: function() {
			var data = {};
			var section = QSection.getSection(this.model);
			var condition = yurt.config.get('condition');
			var refResponse, charCode, character;

			// UNCERTAINTY/INFORMATION SELECTED
			if (section === 'uncertaintyS' || section === 'informationS') {
				
				if (condition === 'control') {
					refResponse = _.find(this.refResponses, function(response) {
						return response.get('name') === 'con-S1';
					});
				} else {
					refResponse = _.find(this.refResponses, function(response) {
						return response.get('name') === 'con-S2';
					});
				}

				if (refResponse) {
					charCode = refResponse.get('code');
					character = yurt.config.getCharacter(charCode);
					data = _.extend(data, character);
				}

			// UNCERTAINTY/INFORMATION BLOCKED
			} else if (section === 'uncertaintyB' || section === 'informationB') {

				if (condition === 'control') {
					refResponse = _.find(this.refResponses, function(response) {
						return response.get('name') === 'con-B1';
					});
				} else {
					refResponse = _.find(this.refResponses, function(response) {
						return response.get('name') === 'con-B2';
					});
				}

				if (refResponse) {
					charCode = refResponse.get('code');
					character = yurt.config.getCharacter(charCode);
					data = _.extend(data, character);
				}
			}

			return data;
		}
	}, {
		getSection: function(question) {
			var section = question.get('group');
			var category = question.get('category');

			if (section === 'uncertainty') {
				if (_.contains(category, 'selected')) {
					return 'uncertaintyS';
				} else {
					return 'uncertaintyB';
				}
			} else if (section === 'information') {
				if (_.contains(category, 'selected')) {
					return 'informationS';
				} else {
					return 'informationB';
				}
			} else if (section === 'theory') {
				if (_.contains(category, 'intelligence')) {
					return 'theoryINT';
				} else {
					return 'theoryPER';
				}
			} else {
				return question.get('group');
			}
		}
	});

	return QSection;

});