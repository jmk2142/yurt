define([
	'jquery',
	'underscore',
	'backbone',
	'views/ProfileEditor',
	'text!tmp/profileMake.ejs',
	'text!tmp/profileHelp.ejs'
], function($, _, Backbone, ProfileEditorView, ProfileMakeTMP, ProfileHelpTMP) {
	'use strict';
	
	var ProfileMake = Backbone.View.extend({
		id: 'profileMake',
		className: 'profileMake',
		template: _.template(ProfileMakeTMP),
		events: {
			'click #help': 'showHelp',
			'click #next': 'gotoNext'
		},
		initialize: function() {
			// console.log('ProfileMake initialized');
			this.subviews = [];

			if (!this.model) {
				var Profile = Parse.Object.extend('Profile');
				this.model = new Profile({
					user: yurt.user,
					pitch: '',
					personal: {
						hobby: '',
						attitude: '',
						trait: ''
					},
					knowledge: ''
				});

				var acl = new Parse.ACL(yurt.user);
						acl.setRoleReadAccess('admin', true);
						acl.setRoleWriteAccess('admin', true);
				this.model.setACL(acl);
			}

			this.listenTo(Backbone, 'requestCSS', this.sendCSS);
		},
		sendCSS: function() {
			var height = this.$('.researchNav').innerHeight() + 30 + 'px';
			Backbone.trigger('inlineCSS', {
				'margin-bottom': height
			});
		},
		render: function() {
			var data = this.model.toJSON();

			var name = yurt.user.get('first') + ' ' + yurt.user.get('last') + "'s";
					name = name.replace(/(s')s|(z')s/g, "$1$2");
			var initials = yurt.user.get('first').toUpperCase()[0] + yurt.user.get('last').toUpperCase()[0];

			data = _.extend(data, {
				initials: initials,
				name: name
			});

			this.$el.html(this.template(data));

			// Add the actual profile editor view
			var view = new ProfileEditorView({
				model: this.model,
				firstProfile: true
			});
			this.subviews.push(view);
			this.profileEditor = view;

			// Start tracking on this view
			view.track();
			this.$('#selfProfile').html(view.render().el);

			if (yurt.config.get('helpProfile') !== 'seen') {
				this.showHelp();
				yurt.config.save({
					'helpProfile': 'seen'
				});
			}

			return this;
		},
		saveInitialState: function() {
			// Saves the INITIAL state of profile
			this.model.save({
				initialPitch: this.model.get('pitch'),
				initialPersonal: this.model.get('personal'),
				initialKnowledge: this.model.get('knowledge')
			});

			// Final state is the most current changed model
		},
		showHelp: function(event) {
			var that = this;

			var data = {
				title: 'Your Social Profile',
				body: ProfileHelpTMP
				// backToSection: this.section
			};

			// Need a handler for when the help / modal closes
			this.listenToOnce(Backbone, 'modal:hidden', function() {
				// yurt.analytics.track('view', {
				// 	section: this.section,
				// 	character: this.character,
				// 	context: ['social']
				// });

				that.profileEditor.track();
			});

			Backbone.trigger('modal:show', data);

			yurt.analytics.track('view', {
				section: 'help',
				character: null,
				context: ['profile']
			});

		},
		gotoNext: function() {
			this.saveInitialState();

			yurt.analytics.clear();

			yurt.config.save('progress', 'social');
			yurt.router.navigate('social', {trigger:true});
		}
	});

	return ProfileMake;

});