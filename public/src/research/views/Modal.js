define([
	'jquery',
	'underscore',
	'backbone',
	'text!tmp/modal.ejs'
], function($, _, Backbone, ModalTMP) {
	'use strict';
	
	var Modal = Backbone.View.extend({
		events: {
			'click [data-section]': 'backToSection'
		},
		template: _.template(ModalTMP),
		initialize: function() {

			this.listenTo(Backbone, 'modal:load', this.load);
			this.listenTo(Backbone, 'modal:show', this.show);
			this.listenTo(Backbone, 'modal:hide', this.hide);
			this.listenTo(Backbone, 'modal:clear', this.clearModal);
			// this.listenTo(Backbone, 'modal:hideAll', this.hideAll);
			this.$el.on('hidden.bs.modal', function() {
				Backbone.trigger('modal:hidden');
			});
		},
		clearModal: function(arg) {
			if (this.$el.data('bs.modal')) {
				this.$el.removeClass('fade');
				this.$el.modal('hide');
				this.$el.addClass('fade');
			}
		},
		render: function(data) {
			data = data || {};
			
			data.title = data.title ? data.title : 'No Title';
			data.backToSection = data.backToSection ? data.backToSection : undefined;

			if (data.view) {
				this.$('.modal-content').html(data.view.render().el);
			} else {

				data.body = data.body ? data.body : 'No Body';
				data.events = data.events ? data.events : {};

				this.delegateEvents(data.events);

				this.$('.modal-content').html(this.template(data));
			}
			
			return this;
		},
		// backToSection: function(event) {
		// 	Backbone.trigger('modal:backToSection:', $(event.currentTarget).data('section'));
		// },
		load: function(data) {
			this.render(data);
		},
		show: function(data, callback) {
			var that = this;

			// Render the inside content of modal
			if (data) {
				this.load(data);
			}

			this.$el.modal('show');
			
		},
		hide: function(options) {
			if (_.has(options, 'animation')) {
				if (options.animation === false) {
					this.$el.removeClass('fade');
					this.$el.modal('hide');
					this.$el.addClass('fade');
				}
			} else {
				this.$el.modal('hide');	
			}
		}
	});

	return Modal;

});