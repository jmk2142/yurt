define([
	'jquery',
	'underscore',
	'backbone'
], function($, _, Backbone) {
	'use strict';

	var AppView = Backbone.View.extend({
		events: {},
		initialize: function() {
			// console.log('AppView created');
			this.listenTo(Backbone, 'inlineCSS', this.inlineCSS);
		},
		render: function() {
			return this;
		},
		showView: function(view) {
			if (this.view) {
				// console.log('removed view');
				this.view.remove();
			}
			this.view = view;
			this.resetCSS();
			this.$el.html(this.view.render().el);

			// Request InlineCSS
			Backbone.trigger('requestCSS');
			Backbone.trigger('shadow', true);

			return this;
		},
		resetCSS: function() {
			this.$el.attr('style','');
		},
		inlineCSS: function(data) {
			this.$el.css(data);
		}
	});

	return AppView;

});
