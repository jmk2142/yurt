define([
	'jquery',
	'underscore',
	'backbone',
	'views/Comment',
	'text!tmp/post.ejs',
	'text!tmp/comment-editor.ejs'
], function($, _, Backbone, CommentView, PostTMP, EditorTMP) {
	'use strict';
	
	var Post = Backbone.View.extend({
		className: 'post',
		template: _.template(PostTMP),
		events: {
			'click .profile': 'profile',
			'click .vote': 'setVote',
			'click .reply': 'reply',
			'click .cancel': 'cancel',
			'click .submit': 'addComment',
			'keypress textarea': 'onKeypress',
			'focus .editor textarea': 'trackReply',
			'blur .editor textarea': 'clearTracking'
		},
		clearTracking: function(event) {
			yurt.analytics.track('view', {
				section: 'conversation',
				character: 'SUB',
				context: ['social']
			});
		},
		trackReply: function(event) {
			var $target = $(event.currentTarget);

			yurt.analytics.track('reply', {
				section: 'conversation',
				character: this.model.get('character'),
				context: [this.model.get('name'), this.model.get('category')],
				watchEl: $target
			});
		},
		initialize: function(options) {
			this.subviews = [];

			var upvoteIDs = _.pluck(this.model.get('upvotes'), 'id');
			var downvoteIDs = _.pluck(this.model.get('downvotes'), 'id');

			if (_.contains(upvoteIDs, yurt.user.id)) {
				this.vote = 'up';
			} else if (_.contains(downvoteIDs, yurt.user.id)) {
				this.vote = 'down';
			} else {
				this.vote = null;
			}

			this.comments = new Parse.Collection(options.comments);
			this.listenTo(this.comments, 'add', this.render);
			// this.listenTo(this.comments, 'remove', this.render);

		},
		render: function() {
			var data = this.model.toJSON();
			var character = yurt.config.getCharacter(data.character);
					data = _.extend(data, {
						initials: character.initials,
						name: character.first + ' ' + character.last,
						color: character.color,
						content: Post.substituteName(data.content),
						code: character.code
					});

			this.$el.html(this.template(data));

			this.addAllComments();

			this.showVote();

			return this;
		},
		profile: function(event) {
			var code = $(event.currentTarget).data('profile');
			Backbone.trigger('profile', code);
		},
		setVote: function(event) {
			var $target = $(event.currentTarget);
			var valence = $target.data('valence');

			if (valence === 'up') {
				this.vote = this.vote !== 'up' ? valence : null;
			} else if (valence === 'down') {
				this.vote = this.vote !== 'down' ? valence : null;
			}

			this.showVote();
			this.saveVote();
		},
		showVote: function() {
			// Reset all vote indicators
			this.$('.vote').attr('class', 'vote');

			if (this.vote === 'up') {
				this.$('.vote[data-valence="up"]').addClass('active');
				this.$('.vote[data-valence="down"]').addClass('inactive');
			} else if (this.vote === 'down') {
				this.$('.vote[data-valence="up"]').addClass('inactive');
				this.$('.vote[data-valence="down"]').addClass('active');
			}
		},
		saveVote: function() {
			var that = this;
			var valence = null;

			if (this.vote === 'up') {
				valence = 'up';
			} else if (this.vote === 'down') {
				valence = 'down';
			}

			Parse.Cloud.run('vote', {
				postID: this.model.id,
				valence: valence
			}).then(function(model) {
				Backbone.trigger('system:push', {
					icon: 'save',
					message: 'Saved Vote',
					urgent: true
				});
			}, function(error) {
				Backbone.trigger('system:push', {
					icon: 'error',
					message: 'Error: ' + error.message,
					urgent: true
				});
			});
			
		},
		reply: function(event) {
			this.showEditor();
			this.toggleActions(event);
		},
		cancel: function(event) {
			this.hideEditor();
			this.toggleActions(event);
		},
		onKeypress: function(event) {
			if (event.which === 13) {
				this.addComment(event);
				console.log(event);
			}
		},
		addComment: function(event) {
			// Save Comment
			var Comment = Parse.Object.extend('Comment');
			var comment = new Comment({
				user: yurt.user,
				author: yurt.user.get('first') + ' ' + yurt.user.get('last'),
				post: this.model,
				message: this.$('textarea').val(),
				status: 'active'
			});
			var acl = new Parse.ACL(yurt.user);
					acl.setRoleReadAccess('admin', true);
					acl.setRoleWriteAccess('admin', true);

			comment.setACL(acl);

			this.comments.create(comment);

			comment.save().then(function(comment) {
				Backbone.trigger('system:push', {
					icon: 'save',
					message: 'Saved Comment',
					urgent: true
				});
			}, function(error) {
				Backbone.trigger('system:push', {
					icon: 'error',
					message: 'Error: ' + error.message,
					urgent: true
				});
			});

			this.hideEditor();
			this.toggleActions(event);
		},
		addAllComments: function() {
			var that = this;

			var fragment = document.createDocumentFragment();

			this.comments.each(function(comment) {
				var itemEl = that.addCommentItem(comment).render().el;
				fragment.appendChild(itemEl);
			});

			this.$('.comments').html(fragment);
		},
		addCommentItem: function(comment) {

			var view = new CommentView({
				model: comment,
			});
			this.subviews.push(view);

			return view;
		},
		showEditor: function() {
			var $editor = $(EditorTMP);
			this.$('.comments').after($editor);
			$editor.find('textarea').focus();
		},
		hideEditor: function() {
			this.$('textarea').val('');
			this.$('.editor').remove();
		},
		toggleActions: function(event) {
			var $target = $(event.currentTarget);
			if ($target.hasClass('reply') || event.type === 'keypress') {
				$(event.currentTarget).addClass('hidden').siblings().removeClass('hidden');
			} else {
				this.$('.cancel, .submit').addClass('hidden');
				this.$('.reply').removeClass('hidden');
			}
		}
	}, {
		substituteName: function(text) {
			var codes = ['SUB', 'CRT', 'PRA', 'NEU'];

			_.each(codes, function(code) {
				var regex = new RegExp("@" + code, "g");
				text = text.replace(regex, yurt.config.getCharacter(code).nick);
						regex = new RegExp("(s')s|(z')s", "g");
				text = text.replace(regex, "$1$2");
			});

			return text;
		}
	});

	return Post;

});


// var query = new Parse.Query('Post');
// 		query.containedIn('upvotes', [yurt.user]);
// query.count().then(function(count) {
// 	console.log('COUNT: ' + count);
// });