define([
	'jquery',
	'underscore',
	'bootstrap',
	'backbone',
	'utils/UserBot'
], function($, _, Bootstrap, Backbone, UserBot) {
	'use strict';

	var NavbarView = Backbone.View.extend({
		events: {
			'click #resume': 'preventDefault',
			'click #noConvo': 'noConversation',
			'click #quitBtn': 'quitStudy'
		},
		initialize: function() {
			// console.log('Navbar initialized');

			// console.log('NavbarView created');
			this.render();

			window.nav = this;

			this.listenTo(Backbone, 'shadow', this.toggleShadow);
		},
		toggleShadow: function(show) {
			if (show) {
				this.$el.removeClass('noShadow');
			} else {
				this.$el.addClass('noShadow');
			}

			this.messageQueue = [];
			this.ready = true;
			this.paused = false;

			this.$systemMsg = this.$('.system-msg');

			this.listenTo(Backbone, 'system:userCount', this.updateUserCount);
			this.listenTo(Backbone, 'system:push', this.pushSystemMsg);
			this.listenTo(Backbone, 'system:clear', this.clearSystemMsg);

			// TEST assignment, remove later
			this.userBot = new UserBot();
			window.x = this;

		},
		updateUserCount: function(count) {
			this.userCount = count;

			if (this.ready) {
				this.showUserCount();
			}
		},
		showUserCount: function() {
			var iconHTML = '<i class="fa fa-users"></i>&nbsp;&nbsp;';
			var text = this.userCount > 1 ? ' users online' : ' user online';
			this.$systemMsg.html(iconHTML + this.userCount + text);
		},
		pushSystemMsg: function(messageObj) {
			var defaults = {
				icon: 'cloud',
				message: 'The weather is nice today.',
				label: undefined,
				duration: 5000,
				persist: false,
				urgent: false
			};
			messageObj = messageObj ? _.extend(defaults, messageObj) : defaults;

			// Only allow queing if the system is not paused
			if (!this.paused) {
				this.messageQueue.push(messageObj);	

				// Make a persistent message
				if (messageObj.persist) {
					messageObj.duration = 1000 * 60 * 60 * 24 * 7; // One week duration...
				}

				// Urgent will clear queue and IMMEDIATELY show message
				if (messageObj.urgent) {
					this.messageQueue = this.messageQueue.slice(this.messageQueue.length - 1);
					clearTimeout(this.currentDelay);
					this.ready = true;
				}

				// Only fire off queue if the system is not cycling through "busy"
				if (this.ready) {
					// Execute first message
					this.showSystemMsg();
				}
			} else {
				console.log('SystemMsg is PAUSED');
			}
			
		},
		showSystemMsg: function() {
			var that = this;
			var icons = {
				'delete': '<i class="fa fa-trash-o"></i>',
				'bullhorn': '<i class="fa fa-bullhorn"></i>',
				'cloud': '<i class="fa fa-cloud"></i>',
				'info':'<i class="fa fa-info"></i>',
				'refresh':'<i class="fa fa-refresh fa-spin"></i>',
				'users':'<i class="fa fa-users"></i>',
				'save':'<i class="fa fa-cloud-upload"></i>',
				'error': '<i class="fa fa-warning"></i>',
				'circle': '<i class="fa fa-circle-o faa-burst faa-slow animated"></i>'
			};

			if (this.messageQueue.length > 0) {

				this.ready = false;

				var current = that.messageQueue.shift();

				var message;
				if (current.label) {
					message = '<span class="label label-' + current.label + '" style="border-radius:1em;font-size:0.85em;">' + icons[current.icon] + '&nbsp;&nbsp;' + current.message + '&nbsp;</span>';
				} else {
					message = icons[current.icon] + '&nbsp;&nbsp;' + current.message;
				} 

				// Start when fadeOut complete
				this.$systemMsg.one('transitionend', function(event) {
					that.$systemMsg.html(message);

					// Start when fadeIn complete
					that.$systemMsg.one('transitionend', function(event) {
						that.currentDelay = _.delay(function() {
							if (that.messageQueue.length > 0) {
								that.showSystemMsg();
							} else {
								// console.log('No more system messages AAAx');
								that.ready = true;
								// that.showUserCount();
								that.clearSystemMsg();
							}
						}, current.duration);
					});
					that.$systemMsg.addClass('in');
				});
				this.$systemMsg.removeClass('in');

			} else { // show default
				console.log('no more messages!');
			}
		},
		clearSystemMsg: function() {
			var that = this;

			// Clear the queue and delay
			this.messageQueue = [];
			clearTimeout(this.currentDelay);

			// Switch to default userCount message
			this.$systemMsg.one('transitionend', function(event) {
				that.showUserCount();
				that.$systemMsg.addClass('in');
			});

			this.$systemMsg.removeClass('in');

		},
		render: function() {
			return this;
		},
		showHelpButton: function() {
			this.$('#help').removeClass('hidden');
		},
		hideHelpButton: function() {
			this.$('#help').addClass('hidden');
		},
		togglePopover: function() {
			console.log('toggle');
			this.$quitPopover.popover('toggle');
		},
		preventDefault: function(event) {
			event.preventDefault();
			console.log('RESUME SELECTED');
		},
		noConversation: function(event) {
			event.preventDefault();
			console.log('NO CONVERSATION SELECTED');
			yurt.config.save('conversation', false);
		},
		quitStudy: function(event) {
			console.log('QUIT SELECTED');
		}
	});

	return NavbarView;

});
