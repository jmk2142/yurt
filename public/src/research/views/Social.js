define([
	'jquery',
	'underscore',
	'backbone',
	'models/Question',
	'views/Post',
	'views/Selection',
	'views/ProfileEditor',
	'collections/QuestionList',
	'text!tmp/social.ejs',
	'text!tmp/carousel.ejs',
	'text!tmp/socialHelp.ejs'
	], function($, _, Backbone, Question, PostView, SelectionView, ProfileEditor, QuestionList, SocialTMP, CarouselTMP, SocialHelpTMP) {
	'use strict';
	
	var SocialView = Backbone.View.extend({
		id: 'social',
		className: 'page social',
		template: _.template(SocialTMP),
		events: {
			'click #profileTabs [data-section]': 'clickSection',
			'click .socialBack [data-section]': 'clickSection',
			'click [data-section="self-profile"]': 'clickSection',
			'click #help': 'showHelp',
			'click #next': 'selectionShow',
			'click #submit-comment': 'saveComment',
			// 'click #editMyProfile': 'showProfileEditor'
		},
		clickX: function(event) {
			console.log('CLICK X');
		},
		sendCSS: function() {
			var height = this.$('.researchNav').innerHeight() + 30 + 'px';
			Backbone.trigger('inlineCSS', {
				'margin-bottom': height
			});
		},
		initialize: function(options) {
			console.log('INITIALIZE Social.js');
			var that = this;

			this.subviews = [];

			this.progress = options.progress;

			this.getModels();

			this.listenTo(Backbone, 'profile', this.showProfile);
			this.listenTo(Backbone, 'selection:hide', this.selectionHidden);
			this.listenTo(Backbone, 'requestCSS', this.sendCSS);
		},
		render: function() {
			var subject = yurt.config.getCharacter('SUB').first + ' ' + yurt.config.getCharacter('SUB').last + '\'s';
			var data = {
				pitch: yurt.config.getProfile('SUB').pitch,
				subject: subject.replace(/(s')s|(z')s/g, "$1$2")
			};
			this.$el.html(this.template(data));

			this.setSection({
				section: 'conversation',
				character: 'SUB'
			});

			this.setupSelection();

			if (yurt.config.get('helpSocial') !== 'seen') {
				this.showHelp();
				yurt.config.save({
					'helpSocial': 'seen'
				});					
			}

			return this;
		},
		showProfile: function(code) {
			var character = yurt.config.getCharacter(code);
			var profile = yurt.config.getProfile(code);

			this.$('.profile-name').html(character.first + ' ' + character.last);

			if (profile.pitch) {
				this.$('#pitch p').html(profile.pitch);
			} else {
				this.$('#pitch p').html('');
			}
			this.$('#personal p').html(yurt.config.getProfile(code, 'personal'));
			this.$('#knowledge p').html(profile.knowledge);

			this.setSection({
				section: 'profile',
				character: code
			});
		},
		getModels: function() {
			var that = this;

			this.postList = new Parse.Collection();
			this.commentList = new Parse.Collection();

			var postQuery = new Parse.Query('Post');
					postQuery.limit(100);
					postQuery.ascending('order');

			this.postList.query = postQuery;

			var commentQuery = new Parse.Query('Comment');
					commentQuery.limit(100);
					commentQuery.ascending('createdAt');
					commentQuery.equalTo('user', yurt.user);
					commentQuery.equalTo('status', 'active');

			this.commentList.query = commentQuery;

			var profileQuery = new Parse.Query('Profile');
					profileQuery.equalTo('user', yurt.user);

			var p1 = this.postList.fetch();
			var p2 = this.commentList.fetch();
			var p3 = profileQuery.first();

			Parse.Promise.when(p1, p2, p3).then(function(postList, commentList, profile) {
				that.addAllPosts(postList, commentList);
				that.addMyProfile(profile);
			});
		},
		addMyProfile: function(profile) {
			this.selfProfile = new ProfileEditor({
				model: profile
			});
			this.subviews.push(this.selfProfile);

			this.$('#selfProfile').prepend(this.selfProfile.render().el);
		},
		addAllPosts: function(postList, commentList) {
			var that = this;

			var fragment = document.createDocumentFragment();

			postList.each(function(post) {
				var comments = commentList.filter(function(comment) {
					if (comment.get('post')) {
						return comment.get('post').id === post.id;						
					} else {
						return false;
					}
				});
				var itemEl = that.addPostItem(post, comments).render().el;
				fragment.appendChild(itemEl);
			});

			this.$('#posts').html(fragment);
		},
		addPostItem: function(post, comments) {

			var view = new PostView({
				model: post,
				comments: comments
			});
			this.subviews.push(view);

			return view;
		},
		// Listen to data-section (tabs)
		clickSection: function(event) {
			var $target, section;
			
			$target = $(event.currentTarget);
			section = $target.data('section');

			this.setSection({
				section: section
			});
		},
		// data-section = profile, personal, knowledge, self-profile
		setSection: function(options) {
			options = options || {};
			options.section = options.section ? options.section : 'conversation';

			// SET CHARACTER
			this.character = options.character ? options.character : this.character;

			// SET SECTION
			this.section = options.section;

			// Conversation section... assign character to SUB
			if (this.section === 'conversation') {

				this.character = 'SUB';

			// Profile section... randomize tab based on user
			} else if (this.section === 'profile') {

				if (this.character === 'SUB') {
					this.section = _.sample(['pitch','personal','knowledge']);
				} else {
					this.section = _.sample(['personal','knowledge']);
				}

			// Self Profile Editor... randomize tab
			} else if (this.section === 'self-profile') {

				this.section = _.sample(['self-pitch','self-personal','self-knowledge']);
				this.character = 'SELF';

				this.selfProfile.setSection({
					section: this.section,
					character: this.character
				});
				
			}

			// ALTERNATE TRACK code
			if (this.section === 'self-profile') {
				this.selfProfile.track();
			} else {
				yurt.analytics.track('view', {
					section: this.section,
					character: this.character,
					context: ['social']
				});
			}

			console.log('setSection: ', this.section, this.character);

			this.toggleSection();
		},
		toggleSection: function() {

			// CONVERSATION SECTION
			if (this.section === 'conversation') {
				this.$('#profile, #selfProfile').addClass('hidden');
				this.$('#conversation').removeClass('hidden');

			// SELF PROFILE SECTION
			} else if (_.contains(['self-pitch','self-personal','self-knowledge'], this.section)) {

				// Just show the section...
				this.$('#profile, #conversation').addClass('hidden');
				this.$('#selfProfile').removeClass('hidden');
			
				// Let the tab handling happen in the ProfileEditor View
				// this.$('#self-profileTabs [data-section="' + this.section + '"]').tab('show');

			// OTHER PROFILE SECTION
			} else {
				this.$('#profile').removeClass('hidden');
				this.$('#conversation, #selfProfile').addClass('hidden');

				// Toggle Pitch Tab
				if (this.character === 'SUB') {
					this.$('[data-target="#pitch"]').parent().removeClass('hidden');
				} else {
					this.$('[data-target="#pitch"]').parent().addClass('hidden');
				}

				this.$('#profileTabs [data-section="' + this.section + '"]').tab('show');
			}

			this.toggleTitle();
		},
		toggleTitle: function() {
			var title = '';
			var character, first, last;
			var label = '<span class="label icon-color-0">Panelist</span>';

			if (this.section === 'conversation') {
				character = yurt.config.getCharacter('SUB');
				first = yurt.config.getCharacter('SUB').first;
				last = yurt.config.getCharacter('SUB').last + "'s";

				title = first + ' ' + last.replace(/(s')s|(z')s/g, "$1$2") + ' Conversation';

			} else if (_.contains(['self-pitch','self-personal','self-knowledge'], this.section)) {				
				first = yurt.user.get('first');
				last = yurt.user.get('last') + "'s";
				title = first + ' ' + last.replace(/(s')s|(z')s/g, "$1$2") + ' Profile';
				character = {
					initials: first.toUpperCase()[0] + last.toUpperCase()[0],
					color: 0
				};
			} else {
				character = yurt.config.getCharacter(this.character);
				first = yurt.config.getCharacter(this.character).first;
				last = yurt.config.getCharacter(this.character).last + "'s";

				title = first + ' ' + last.replace(/(s')s|(z')s/g, "$1$2") + ' Profile';

				if (character.code === 'SUB') {
					// title = title + ' &mdash; ' + '<span class="yurt-label icon-color-' + character.color + '">Participant</span>';
					title = title + ' <i class="fa fa-long-arrow-right"></i> ' + '<span class="yurt-label icon-color-' + character.color + '">Participant</span>';
				} else {
					// title = title + ' &mdash; ' + '<span class="yurt-label icon-color-0 yurt-label-bordered">Panelist</span>';
					title = title + ' <i class="fa fa-long-arrow-right"></i> ' + '<span class="yurt-label icon-color-0 yurt-label-bordered">Panelist</span>';
				}

				// label = '<div class="label icon-color-' + character.color + '">Participant</div>';
			}

			this.$('.title').html(title);

			// Toggle Icon
			var initials = character.initials;
			var color = character.color;
			this.$('.socialNav .userIcon').attr('class', 'userIcon icon-color-' + color).html(initials);
		},
		showHelp: function(event) {
			var data = {
				title: 'Things You Need To Do ...',
				// body: CarouselTMP,
				body: SocialHelpTMP,
				backToSection: this.section
			};

			// Need a handler for when the help / modal closes
			this.listenToOnce(Backbone, 'modal:hidden', function() {
				console.log(this.section);

				if (_.contains(['self-pitch','self-personal','self-knowledge'], this.section)) {
					this.selfProfile.track();
				} else {
					yurt.analytics.track('view', {
						section: this.section,
						character: this.character,
						context: ['social']
					});
				}


			});

			Backbone.trigger('modal:show', data);

			yurt.analytics.track('view', {
				section: 'help',
				character: null,
				context: ['social']
			});

		},
		setupSelection: function() {
			var that = this;

			var questionIDs = yurt.config.get('battery').decision;
			var questions = _.chain(questionIDs).map(function(id) {
				return Question.createWithoutData(id);
			}).value();

			var questionList = new QuestionList(questions, {
				comparator: function(model) {
					return yurt.config.get('battery').decision.indexOf(model.id);
				}
			});
					// questions.setMode('decision');

			// Get this user's responses
			var responseQuery = new Parse.Query('Response');
					responseQuery.equalTo('user', yurt.user);
					responseQuery.equalTo('battery', 'decision');

			// var p1 = questions.fetch();
			var p1 = Parse.Object.fetchAll(questionList.models);
			var p2 = responseQuery.find();

			Parse.Promise.when(p1, p2).then(function(questions, responses) {
				var view = new SelectionView({
					el: that.$('#selection'),
					collection: questionList,
					responses: responses,
					progress: that.progress
				});

				that.subviews.push(view);

				that.$('#selection').modal({
					show: false,
					backdrop: 'static',
					keyboard: false
				});

				view.render();
			});

		},
		selectionShow: function(event) {
			Backbone.trigger('selection:show');
		},
		selectionHidden: function(data) {
			var data = data || {};
			var context = ['social'];

			yurt.analytics.track('view', {
				section: this.section,
				character: this.character,
				context: data.context ? _.union(context, data.context) : context
			});
		},
		onClose: function() {
			console.log('SOCIAL onClose');
			Backbone.trigger('modal:clear');
			// yurt.analytics.destroy();
		}
	});

	return SocialView;

});