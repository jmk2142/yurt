define([
	'jquery',
	'underscore',
	'backbone',
	'views/Question',
	'text!tmp/outcome.ejs'
], function($, _, Backbone, QuestionView, OutcomeTMP) {
	'use strict';
	
	var Outcome = Backbone.View.extend({
		id: 'outcome',
		className: 'outcome',
		template: _.template(OutcomeTMP),
		events: {
			'click #next': 'gotoNext'
		},
		initialize: function(options) {
			var that = this;
			this.subviews = [];
			console.log('Outcome initialized');
			console.log(this.model);

			var selectedRes = options.selectedRes;
			var blockedRes = options.blockedRes;

			this.selected = yurt.config.getCharacter(selectedRes.get('code'));
			this.blocked = yurt.config.getCharacter(blockedRes.get('code'));

			this.battery = options.battery;

			// Get this user's responses
			var responseQuery = new Parse.Query('Response');
					responseQuery.equalTo('user', yurt.user);
					responseQuery.equalTo('battery', this.battery);
			
			responseQuery.find().then(function(responses) {
				that.responses = responses;

				// Assign response to each q view, then run a markResponse
				_.each(responses, function(response) {
					// Find the subview
					var qSubview = _.find(that.subviews, function(view) {
						return view.id === response.get('question').get('id');
					});

					if (qSubview) {
						qSubview.response = response;
						qSubview.markResponse();
					}
				});
			});

			Backbone.trigger('system:clear');

			yurt.analytics.track('view', {
				section: 'survey',
				character: this.selected.code,
				context: ['rejection']
			});
		},
		render: function() {
			var data = {
				selected: {
					fullName: this.selected.first + ' ' + this.selected.last,
					first: this.selected.first,
					initials: this.selected.initials,
					color: this.selected.color
				},
				blocked: {
					fullName: this.blocked.first + ' ' + this.blocked.last,
					first: this.blocked.first,
					initials: this.blocked.initials,
					color: this.blocked.color
				}
			};
			this.$el.html(this.template(data));

			this.addQuestion(this.model);

			return this;
		},
		addQuestion: function(question) {
			var view = new QuestionView({
				model: this.model,
				battery: this.battery,
				reference: {
					selected: this.selected,
					blocked: this.blocked
				}
			});
			this.subviews.push(view);
			this.$('#questionItem').html(view.render().el);
		},
		gotoNext: function(event) {
			event.preventDefault();

			yurt.analytics.clear();

			// NEED TO CHECK MAKE SURE RESPONSE IS GOOD

			yurt.config.save('progress', 'debrief').then(function(config) {
				window.location.href = 'complete';
			});
			

			// yurt.router.navigate('start', {trigger:true});
		}
	});

	return Outcome;

});