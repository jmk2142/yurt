define([
	'jquery',
	'underscore',
	'backbone',
	'views/Question',
	'collections/QuestionList',
	'text!tmp/selection.ejs'
], function($, _, Backbone, QuestionView, QuestionList, SelectionTMP) {
	'use strict';
	
	var Selection = Backbone.View.extend({
		className: 'selection',
		template: _.template(SelectionTMP),
		events: {
			'click .selection i': 'selection',
			'click [data-toggle="tab"]': 'activateTab',
			'click .cancel-question': 'hideModal',
			'click .back-question': 'back',
			'click .next-question': 'next'
		},
		clearModal: function(arg) {
			if (this.$el.data('bs.modal')) {
				this.$el.removeClass('fade');
				this.$el.modal('hide');
				this.$el.addClass('fade');
			}
		},
		activateTab: function(event) {
			// Highlight tab
			this.$('.active').removeClass('active');
			$(event.currentTarget).addClass('active');

			this.track();

			// Add selected color
			var color = $(event.currentTarget).data('color');
			this.$('.left').attr('class', 'left border-color-' + color);
			this.$('.right').attr('class', 'right border-color-' + color);
			
			// Update Right Column Heading
			var first = $(event.currentTarget).data('first').concat("'s");
			this.$('.struggleName').html(first.replace(/(s')s|(z')s/g, "$1$2"));
		},
		track: function(event) {
			var qSet = this.qSet[this.questionIndex];
			var context, q1, q2;

			if (qSet.length > 1) {
				q1 = qSet[0];
				q2 = qSet[1];
				context = [q1.get('name'),q2.get('name')];

			} else {
				q1 = qSet[0];
				context = [q1.get('name')];
			}

			if (this.struggle) {
				yurt.analytics.track('view', {
					section: 'struggle',
					character: event ? $(event.currentTarget).data('code') : this.$('.struggle .user.active').data('code')
				});
			} else {
				yurt.analytics.track('edit', {
					section: 'selection',
					character: null,
					context: _.union(['social'], context)
				});
			}
		},
		showSelection: function(data) {
			// var qSet = this.qSet[this.questionIndex];
			// var context, q1, q2;

			// if (qSet.length > 1) {
			// 	q1 = qSet[0];
			// 	q2 = qSet[1];
			// 	context = [q1.get('name'),q2.get('name')];

			// } else {
			// 	q1 = qSet[0];
			// 	context = [q1.get('name')];
			// }

			this.$el.modal('show');

			this.track();

			// if (this.struggle) {
			// 	yurt.analytics.track('view', {
			// 		section: 'struggle',
			// 		character: this.$('.struggle .user.active').data('code')
			// 	});
			// } else {
			// 	yurt.analytics.track('view', {
			// 		section: 'selection',
			// 		character: null,
			// 		context: _.union(['social'], context)
			// 	});
			// }

			console.log(this);
		},
		initialize: function(options) {
			var that = this;
			this.subviews = [];

			this.condition = yurt.config.get('condition');
			this.responses = options.responses;
			this.progress = options.progress;
			this.struggle = false;
			this.struggleAfterIndex = 2;
			this.makeQSets();
			
			if (this.progress) {
				this.questionIndex = this.getQIndexByQID(this.progress);
			} else {
				this.questionIndex = 0;
			}

			this.listenTo(Backbone, 'selection:show', this.showSelection);
			this.listenTo(Backbone, 'modal:clear', this.clearModal);
		},
		render: function() {
			var that = this;
			var data;
			var view, viewSelect, viewBlock;
			var resSelected, resBlocked;
			var set;
			
			// Remove all child views
			_.each(this.subviews, function(view) {
				view.remove();
			});

			var panelists = [];
			_.each(yurt.config.get('panelistOrder'), function(code) {
				var character = yurt.config.getCharacter(code);
						character = _.extend(character, {
							struggle: yurt.config.getProfile(code).struggle
						});

				panelists.push(character);
			});
			
			// Struggle Page - NO QUESTION
			if (this.struggle) {
				data = {
					type: 'struggle',
					title: 'Please read the following updated information',
					panelists: panelists
				};
				this.$('.modal-content').html(this.template(data));
				yurt.config.save('progress', 'social/struggle');

			// Questions...
			} else {
				set = this.qSet[this.questionIndex];

				// Panelist Selection Mode
				if (set.length > 1) {

					resSelected = this.findResponse(set[0]);
					resBlocked = this.findResponse(set[1]);

					data = {
						type: 'multiSet',
						title: 'Request to Connect',
						q1: set[0].toJSON(),
						q2: set[1].toJSON(),
						panelists: panelists,
						selected: undefined,
						blocked: undefined
					};

					// SET default view options
					var optionsSelect = {
						model: set[0],
						battery: 'decision',
						response: resSelected ? resSelected : undefined,
						parent: this
					};

					var optionsBlock = {
						model: set[1],
						battery: 'decision',
						response: resBlocked ? resBlocked : undefined,
						parent: this
					};

					// Extend reference response if necessary
					if (set[0].get('name') === 'con-S2') {
						var selectedRefRes = this.findRefResponse('con-S1');
						optionsSelect = _.extend(optionsSelect, {
							reference: selectedRefRes
						});
						// Reference response gives me the code
						data = _.extend(data, {
							selected: yurt.config.getCharacter(selectedRefRes.get('code'))
						});
					}
					if (set[1].get('name') === 'con-B2') {
						var blockedRefRes = this.findRefResponse('con-B1');
						optionsBlock = _.extend(optionsBlock, {
							reference: blockedRefRes
						});
						data = _.extend(data, {
							blocked: yurt.config.getCharacter(blockedRefRes.get('code'))
						});
					}

					viewSelect = new QuestionView(optionsSelect);
					viewBlock = new QuestionView(optionsBlock);

					this.subviews.push(viewSelect);
					this.subviews.push(viewBlock);

					// Data should have selected and blocked characters
					this.$('.modal-content').html(this.template(data));

					this.$('#questionItems').append(viewSelect.render().el);
					this.$('#questionItems').append(viewBlock.render().el);

					// Selected response found
					if (resSelected) {
						this.updateSelection(resSelected, 'selected');
					} else {
						this.responses.push(viewSelect.response);
					}
					// Blocked response found
					if (resBlocked) {
						this.updateSelection(resBlocked, 'blocked');
					} else {
						this.responses.push(viewBlock.response);
					}

					// Update Progress
					// yurt.config.save('progress', 'social/' + set[1].id);

				// Default Selection Mode (Single Question)
				} else {

					data = {
						type: 'singleSet',
						title: 'Please answer the following questions',
						q1: set[0].toJSON(),
						selected: undefined,
						blocked: undefined
					};

					var options = {
						model: set[0],
						battery: 'decision',
						response: this.findResponse(set[0])
					};

					var refLookup = {
						'conInfo-S1': 'con-S1',
						'conInfo-B1': 'con-B1',
						'conInfo-S2': 'con-S2',
						'conInfo-B2': 'con-B2',
						'conChange-S': ['con-S1','con-S2'],
						'conChange-B': ['con-B1','con-B2']
					};

					var refQuesName = refLookup[set[0].get('name')];
					var refQuestion, refResponse;

					if (_.isArray(refQuesName)) {

						// Array of reference question names
						var refQuesSelect = this.collection.find(function(question) {
							return question.get('name') === refQuesName[0];
						});
						var refQuesBlock = this.collection.find(function(question) {
							return question.get('name') === refQuesName[1];
						});

						var refResSelect = this.findRefResponse(refQuesName[0]);
						var refResBlock = this.findRefResponse(refQuesName[1]);

						options = _.extend(options, {
							reference: [refResSelect, refResBlock]
						});

						if (set[0].get('name') === 'conChange-S') {
							data = _.extend(data, {
								selected: yurt.config.getCharacter(refResSelect.get('code'))
							});
						} else if (set[0].get('name') === 'conChange-B') {
							data = _.extend(data, {
								blocked: yurt.config.getCharacter(refResBlock.get('code'))
							});
						}


					} else {

						refQuestion = this.collection.find(function(question) {
							return question.get('name') === refQuesName;
						});
						refResponse = this.findRefResponse(refQuesName);

						options = _.extend(options, {
							reference: refResponse
						});

						if (_.contains(refQuestion.get('category'), 'selected')) {
							// Reference response gives me the code
							data = _.extend(data, {
								selected: yurt.config.getCharacter(refResponse.get('code'))
							});
						} else if (_.contains(refQuestion.get('category'), 'blocked')) {
							// Reference response gives me the code
							data = _.extend(data, {
								blocked: yurt.config.getCharacter(refResponse.get('code'))
							});
						}

					}

					view = new QuestionView(options);
					this.subviews.push(view);

					this.responses.push(view.response);

					this.$('.modal-content').html(this.template(data));
					this.$('#questionItems').prepend(view.render().el);

				}
			}

			// Set Progress
			if (this.struggle) {
				yurt.config.save('progress', 'social/struggle');
			} else if (this.questionIndex > 0) {
				console.log('SAVED progress');
				yurt.config.save('progress', 'social/' + set[0].id);
			}

			// Open Modal
			if (this.questionIndex > 0 && !this.$el.hasClass('in')) {
				this.$el.on('shown.bs.modal', function() {
					console.log('B shown on');
					if (set && set[0].get('primary').type === 'open') {
						console.log('FOCUS THIS MOTHER');
						view.$('textarea').focus();
					}
					that.$el.off('shown.bs.modal');
				});
				// this.$el.modal('show');
				this.showSelection();
			} else {
				if (set && set[0].get('primary').type === 'open') {
					console.log('FOCUS THIS MOTHER');
					view.$('textarea').focus();
				}
			}

			// console.log('questionIndex: ', this.questionIndex);

			this.toggleActions(set);

			return this;
		},
		getQIndexByQID: function(progressQID) {
			var that = this;
			var index = 0;

			if (progressQID === 'struggle') {
				this.struggle = true;
				index = this.struggleAfterIndex;
			} else {
				// Break once we find the question
				_.each(this.qSet, function(set, i) {
					var found = _.find(set, function(question) {
						return question.id === that.progress;
					});

					if (found) {
						index = i;
					}
				});
			}

			return index;
		},
		// Handles 'input' clicks
		selection: function(event) {
			var that = this;
			var $target = $(event.currentTarget);

			// Triggers selection event
			this.trigger('selection', {
				action: $target.data('action'),
				code: $target.data('code')
			});

			this.render();
		},
		// Updates selection based on input value
		updateSelection: function(response, selection) {

			var iconOff = 'fa fa-2x fa-square-o';
			var iconSelected = 'fa fa-2x fa-check';
			var iconBlocked = 'fa fa-2x fa-close';
			var icons = iconOff;

			if (selection === 'selected') {
				icons = iconSelected;
			} else if (selection === 'blocked') {
				icons = iconBlocked;
			}

			this.$('i[data-action="' + selection + '"]').attr('class', iconOff);
			this.$('i[data-code="' + response.get('code') + '"][data-action="' + selection + '"]').attr('class', icons);
		},
		// Returns a Parse.Object<Response>
		findResponse: function(question) {
			return _.find(this.responses, function(response) {
				return question.id === response.get('question').id;
			});
		},
		findRefResponse: function(refQuesName) {
			var refQuestion = this.collection.find(function(question) {
				return question.get('name') === refQuesName;
			});

			return _.chain(this.responses).find(function(response) {
				return refQuestion.id === response.get('question').id;
			}).value();
		},
		makeQSets: function() {
			this.qSet = _.chain(this.collection.models).groupBy(function(question) {
				return Math.floor(question.get('order'));
			}).toArray().value();
		},
		back: function() {
			console.log(this.questionIndex);

			// Currently showing struggle stories
			if (this.struggle) {
				this.struggle = false;
				this.questionIndex--;
				this.render();

				this.track();
			} else {

				// Show Struggle (when questionIndex is 1 greatr than struggleAfter index)
				if (this.questionIndex === this.struggleAfterIndex + 1 && this.condition === 'struggle') {
					this.struggle = true;
					this.questionIndex--;
					this.render();

					this.track();
				} else {
					if (this.questionIndex > 0) {
						this.questionIndex--;
						this.render();

						this.track();
					} else {
						this.hideModal();
					}
					if (this.questionIndex === 0) {
						yurt.config.save('progress', 'social');	
					}
				}
			}
		},
		next: function() {
			var error = this.isInvalid();

			if (error) {
				this.updateError(error.message);

			// Is NOT invalid, proceed with NEXT
			} else {
				this.updateError();

				if (this.struggle) {
					this.struggle = false;
					this.questionIndex++;
					this.render();

					this.track();
				} else {

					if (this.questionIndex === this.struggleAfterIndex && this.condition === 'struggle') {
						this.struggle = true;
						this.render();

						this.track();
					} else {
						this.questionIndex++;
						if (this.questionIndex < this.qSet.length) {
							this.render();
							console.log('showing: ', this.questionIndex);

							this.track();
						} else {
							console.log('KILL MODAL - Measure B');
							this.$el.on('hidden.bs.modal', function(event) {
								console.log('analytics clear on selection next');
								yurt.analytics.clear();
								
								yurt.config.save('progress', 'survey/measureB');
								yurt.router.navigate('survey/measureB', {trigger:true});
							});
							this.hideModal();
						}
					}
				}

			}

		},
		toggleActions: function(set) {
			// console.log(set);
			var setName = set ? set[0].get('name') : 'struggle';
			var mode = 'all';

			var buttonMode = {
				all: [],
				backOnly: ['con-S2','con-B2'],
				nextOnly: ['struggle','conChange-S','conChange-B','conInfo-S2','conInfo-B2'],
				returnOnly: ['con-S1','con-B1','conInfo-S1','conInfo-B1']
			};
			
			_.every(buttonMode, function(set, key, obj) {
				if (_.contains(set, setName)) {
					mode = key;
					return false;
				} else {
					return true;
				}
			});

			if (mode === 'all') {
				this.$('.back-question, .cancel-question').show();
			} else if (mode === 'backOnly') {
				this.$('.cancel-question').hide();
			} else if (mode === 'nextOnly') {
				this.$('.back-question, .cancel-question').hide();
			} else if (mode === 'returnOnly') {
				this.$('.back-question').hide();
			}
		},
		hideModal: function() {
			var qSet = this.qSet[this.questionIndex];
			var data, q1, q2;

			if (qSet) {
				if (qSet.length > 1) {
					q1 = qSet[0];
					q2 = qSet[1];
					data = {
						context: [q1.get('name'),q2.get('name')]
					};
				} else {
					q1 = qSet[0];
					data = {
						context: [q1.get('name')]
					};
				}
			} else {
				data = {};
			}

			Backbone.trigger('selection:hide', data);
			
			this.$el.modal('hide');
		},
		isInvalid: function() {

			var invalid = true;
			var icons = {
				warning: '<i class="fa fa-warning"></i>'
			};
			var messages = {
				select: 'Please select one panelist',
				block: 'Please block one panelist'
			};
			var error = {};

			var set = this.qSet[this.questionIndex];
			var categories = set[0].get('category');
			var question = set[0];

			// Panelist Selection Questions
			if (_.contains(categories, 'selection')) {

				var resSelected = this.findResponse(set[0]);
				var resBlocked = this.findResponse(set[1]);

				if (!resSelected.get('response')) {
					error.message = icons.warning + ' ' + messages.select;
				} else if (!resBlocked.get('response')) {
					error.message = icons.warning + ' ' + messages.block;
				} else {
					invalid = false;
				}

			} else {
				invalid = false;
			}

			return invalid ? error : false;
		},
		updateError: function(message) {
			this.$('.alert').addClass('hidden').html('');

			if (message) {
				this.$('.alert').removeClass('hidden').html(message);
			}
		},
		onClose: function() {
			Backbone.trigger('modal:clear');
		}
	});

	return Selection;

});