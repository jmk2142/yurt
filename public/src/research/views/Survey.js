define([
	'jquery',
	'underscore',
	'backbone',
	'views/Question',
	'views/QSection',
	'text!tmp/survey.ejs'
], function($, _, Backbone, QuestionView, QSection, SurveyTMP) {
	'use strict';
	
	var SurveyView = Backbone.View.extend({
		id: 'survey',
		className: 'page survey',
		template: _.template(SurveyTMP),
		events: {
			// 'click #toggleQuestions': 'toggleQuestions',
			'click #hideCompleted': 'hideCompleted',
			'click #showAll': 'showAll',
			'click #next': 'gotoNext'
		},
		initialize: function(options) {
			var that = this;

			this.battery = options.battery;

			this.subviews = [];
			this.subsections = [];
			// this.allShown = true;

			this.percentComplete = 0;
			this.firstTime = true;

			// Get this user's responses
			var responseList = new Parse.Query('Response');
					responseList.equalTo('user', yurt.user);
					responseList.equalTo('battery', this.battery);

			// Get this users's reference responses
			var referenceList = new Parse.Query('Response');
					referenceList.equalTo('user', yurt.user);
					referenceList.equalTo('battery', 'decision');

			// var p1 = this.collection.fetch();
			var p1 = Parse.Object.fetchAll(this.collection.models);
			var p2 = responseList.find();
			var p3 = referenceList.find();

			this.listenTo(this.collection, 'fetched', this.addAllItems);
			this.listenTo(Backbone, 'check:previous', this.checkPrevious);

			this.listenTo(Backbone, 'question:answered', this.updateCompleted);

			// Make parallel fetch
			Parse.Promise.when(p1, p2, p3).then(function(questions, responses, refResponses) {
				that.responses = responses;
				that.refResponses = refResponses;
				that.collection.trigger('fetched');
				that.updateCompleted();
			}, function(error) {
				if (error && error.message) {
					that.$('#loadingQs').html('<i class="fa fa-exclamation-circle faa-burst faa-slow animated"></i>&nbsp;&nbsp;' + error.message);
				} else {
					that.$('#loadingQs').html('<i class="fa fa-exclamation-circle faa-burst faa-slow animated"></i>&nbsp;&nbsp;' + 'Problem, please try reloading the page.');
				}
				console.log(error.message);
			});

			this.listenTo(Backbone, 'requestCSS', this.sendCSS);

			// TRACK this page
			yurt.analytics.track('view', {
				section: 'survey',
				character: null,
				context: [this.battery]
			});
		},
		render: function() {
			var data = {
				battery: this.battery
			};
			this.$el.html(this.template(data));

			this.updateInstructions();
			this.updateResearchNav();

			return this;
		},
		hideCompleted: function(event) {
				
			var completedQs = _.chain(this.responses).filter(function(response) {
				if (response.get('group') === 'confidence') {
					return response.get('score') ? response : false;
				} else {
					return response;
				}
			}).map(function(response) {
				return response.get('question');
			}).value();

			var IDs = _.pluck(completedQs, 'id');
			var IDstr = '';
			_.each(IDs, function(id, index, list) {
				IDstr += ('#' + id);
				IDstr += index < (list.length - 1) ? ',' : '';
			});

			this.$(IDstr).addClass('hidden');

			_.defer(function() {
				Backbone.trigger('questions:toggled');	
			});
		},
		showAll: function(event) {
			this.$('.question.hidden').removeClass('hidden');

			_.defer(function() {
				Backbone.trigger('questions:toggled');	
			});
		},
		toggleQuestions: function(event) {
			// Go through responses, check if they are completed
			// Filter the completed responses
			// Grab the question id for completed responses
			// Hide the questions that are completed

			if (this.allShown) { // Hide completed
				
				var completedQs = _.chain(this.responses).filter(function(response) {
					if (response.get('group') === 'confidence') {
						return response.get('score') ? response : false;
					} else {
						return response;
					}
				}).map(function(response) {
					return response.get('question');
				}).value();

				var IDs = _.pluck(completedQs, 'id');
				var IDstr = '';
				_.each(IDs, function(id, index, list) {
					IDstr += ('#' + id);
					IDstr += index < (list.length - 1) ? ',' : '';
				});

				this.$(IDstr).addClass('hidden');

				this.allShown = false;

				$(event.currentTarget).html('Show All Items');

			} else { // Show all
				this.$('.question.hidden').removeClass('hidden');
				this.allShown = true;
				$(event.currentTarget).html('Hide Completed Items');
			}

			_.defer(function() {
				Backbone.trigger('questions:toggled');	
			});
		},
		sendCSS: function() {
			var height = this.$('.researchNav').innerHeight() + 30 + 'px';
			Backbone.trigger('inlineCSS', {
				'margin-bottom': height
			});
		},
		updateCompleted: function(options) {
			console.log('updateCompleted');
			options = options || {};

			if (options.response) {
				var found = _.find(this.responses, function(response) {
					return response.id === options.response.id;
				});

				if (!found) {
					this.responses.push(options.response);	
				}
			}

			var completed = _.countBy(this.responses, function(response) {
				if (response.get('group') !== 'confidence') {
					console.log('not confidence');
					return 'complete';
				} else {
					console.log('confidence');
					return response.get('score') ? 'complete' : 'incomplete';
				}
			});

			var totalCount = this.collection.length;
			var completeCount = completed.complete;
			var percent = Math.floor(completeCount / totalCount * 100);

			this.percentComplete = percent ? percent : 0;

			this.$('.percent').html(this.percentComplete);
			
			var $progressBar = $('.progress-bar');

			$progressBar.removeClass('progress-bar-danger progress-bar-info progress-bar-success progress-bar-warning');
			
			if (this.percentComplete === 0) {
				$progressBar.addClass('progress-bar-danger');
			} else if (this.percentComplete === 100) {
				$progressBar.addClass('progress-bar-success');
			} else if (this.percentComplete > 80) {
				$progressBar.addClass('progress-bar-warning');
			}

			this.$('.progress-bar').css({
				width: this.percentComplete + '%'
			});

			if (this.battery === 'measureB') {
				this.updatePanelistStatus();
			}
		},
		updatePanelistStatus: function(event) {
			console.log('updatePanelistStatus');
			
			if (!this.panelistState) {
				var PtREVIEW = 20;		// start at 20%;
				var PtCOMPLETE = 65;	// start at 65%;

				this.panelistState = {
					notify: {showed:false, endPoint: PtREVIEW},
					review: {showed:false, endPoint: PtCOMPLETE},
					response: {showed:false, endPoint: 100}
				};
			}

			var ps = this.panelistState;

			var minutes = 3;
			var adjDelay = Math.floor(Math.random() * (1000 * 60 * minutes));

			if (this.percentComplete < ps.notify.endPoint && !ps.notify.showed) {

				_.delay(function() {
					Backbone.trigger('system:push', {
						icon: 'refresh',
						message: 'Notifying Panelist',
						urgent: true,
						persist: true,
						label: 'info'
					});
				}, this.firstTime ? 0 : adjDelay);

				ps.notify.showed = true;
				this.firstTime = false;

			} else if (this.percentComplete >= ps.notify.endPoint && this.percentComplete < ps.review.endPoint && !ps.review.showed) {
				console.log('triggered');

				_.delay(function() {
					Backbone.trigger('system:push', {
						icon: 'circle',
						message: 'Request is being reviewed',
						urgent: true,
						persist: true,
						label: 'warning'
					});
				}, this.firstTime ? 0 : adjDelay);

				ps.review.showed = true;
				this.firstTime = false;

			} else if (this.percentComplete >= ps.review.endPoint && !ps.response.showed) {

				_.delay(function() {
					Backbone.trigger('system:push', {
						icon: 'bullhorn',
						message: 'Response Available',
						urgent: true,
						persist: true,
						label: 'default'
					});
				}, this.firstTime ? 0 : adjDelay);

				ps.response.showed = true;
				this.firstTime = false;

			} else {
				console.log('huh?');
			}
		},
		checkPrevious: function(answeredQID) {
			var answeredIndex, prevView;
			
			answeredIndex = _.findIndex(this.collection.models, function(question) {
				return question.id === answeredQID;	
			});

			// var sectionCount = _.countBy(this.subviews, function(view) {
			// 	return _.has(view, 'section') ? 'section' : 'question';
			// });
			// console.log(sectionCount);

			if (answeredIndex !== 0) {
			// if (answeredIndex > sectionCount.section - 1) {
				prevView = this.subviews[answeredIndex - 1];
				// prevView = !_.has(this.subviews[answeredIndex - 1], 'section') ? this.subviews[answeredIndex - 1] : this.subviews[answeredIndex - 2];

				var group = prevView.response.get('group');
				var score = prevView.response.get('score');

				console.log(group, score);

				if (prevView.response.isNew() || (group === 'confidence' && score === null)) {
					console.log('not answered');
					prevView.hintSkipped();
				} else {
					console.log('answered');
				}
			}
		},
		updateInstructions: function() {
			var heading = 'Default';
			var instructions = 'Default instructions.';

			console.log(this.battery);

			switch (this.battery) {
				case 'demographic':
					heading = "Let's start with a few basic questions about you.";
					instructions = "Proceed to the next section when complete.";
					break;
				case 'measureA':
					heading = "Just a few more before we get started...";
					instructions = "Proceed to the next section when complete.";
					break;
				case 'measureB':
					heading = "While we wait, tell us a little bit more...";
					instructions = "Proceed to the next section when complete.";
					break;
				default:
					break;
			}

			// Page .yurt-header > h2
			this.$('.page-heading').html(heading);

			// Page .yurt-header > div.page-instructions
			this.$('.page-instructions').html(instructions);
		},
		updateResearchNav: function() {
			var heading = 'Default';
			var items = ['Item 1','Item 2','Item 3'];
			var liTMP = _.template('<li><%= listItem %></li>');
			
			if (this.battery === 'demographic') {
				heading = 'Basic Demographics';
				items = [
					'Please answer each item',
					'Proceed to the next section when complete'
				];
			} else if (this.battery === 'measureA') {
				heading = 'Questionnaire';
				items = [
					'Please answer each item',
					'Proceed to the next section when complete',
					'We\'ll work on your social profile next'
				];
			} else if (this.battery === 'measureB') {
				heading = 'Questionnaire';
				items = [
					'Please answer each item',
					'Proceed to the next section when complete'
				];
			}

			this.$('.researchNav .heading').html(heading);

			var itemsHTML = '';
			_.each(items, function(item) {
				itemsHTML += liTMP({listItem: item});
			});

			this.$('.researchNav ol').html(itemsHTML);
		},
		addAllItems: function() {
			var that = this;

			var fragment = document.createDocumentFragment();

			var section;

			_.each(this.collection.models, function(question, index, list) {

				if (QSection.getSection(question) !== section) {
					section = QSection.getSection(question);

					var qSectionView = new QSection({
						model: question,
						refResponses: that.refResponses
					});
					that.subsections.push(qSectionView);

					var qSectionEl = qSectionView.render().el;
					fragment.appendChild(qSectionEl);
				}

				var itemEl = that.addItem(question).render().el;
				fragment.appendChild(itemEl);
			});

			this.$('#questions').html(fragment);
		},
		addItem: function(question) {
			var userResponse = this.findResponse(question);

			var view = new QuestionView({
				model: question,
				collection: this.collection,
				response: userResponse,
				battery: this.battery
			});
			this.subviews.push(view);

			return view;
		},
		findResponse: function(question) {
			return _.find(this.responses, function(response) {
				return question.id === response.get('question').id;
			});
		},
		onClose: function() {
			_.each(this.subsections, function(view) {
				view.remove();
			});
			this.subsections = null;
		},
		gotoNext: function() {
			var progress = yurt.config.get('progress');
			var next = 'default';
			switch (progress) {
				case 'start': next = 'survey/demographic'; break;
				case 'survey/demographic': next = 'survey/measureA'; break;
				case 'survey/measureA': next = 'profile'; break;
				
				case 'profile': next = 'social'; break;
				case 'social': next = 'survey/decision'; break;
				case 'survey/decision': next = 'survey/measureB'; break;
				case 'survey/measureB': next = 'outcome'; break;
				default: next = 'start'; break;
			}

			yurt.config.save('progress', next);
			yurt.router.navigate(next, {trigger:true});
		}
	});

	return SurveyView;

});