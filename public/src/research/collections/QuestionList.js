define([
	'underscore',
	'parse',
	'models/Question'
], function(_, Parse, Question) {
	'use strict';

	var QuestionList = Parse.Collection.extend({
		model: Question,
		initialize: function(options) {
			// this.setComparator(options.battery);
		},
		setComparator: function(battery) {
			this.comparator = function(model) {
				return yurt.config.get('battery')[battery].indexOf(model.id);
			};
		}
		// setMode: function(battery) {
		// 	this.setQueryMode(battery);
		// 	this.setSortMode(battery);
		// },
		// setQueryMode: function(battery) {
		// 	var condition = yurt.config.get('condition');
		// 	// Build the survey collection
		// 	var query = new Parse.Query('Question');
		// 			query.containedIn('battery', [battery]);

		// 	if (condition === 'struggle') {
		// 		query.containedIn('condition', ['both','struggle']);
		// 	} else {
		// 		query.equalTo('condition', 'both');
		// 	}
		// 	this.query = query;
		// },
		// setSortMode: function(battery) {
		// 	this.comparator = function(model) {
		// 		return yurt.config.get('battery')[battery].indexOf(model.id);
		// 	};
		// }
	});

	return QuestionList;
});