define([
	'underscore',
	'parse'
], function(_, Parse) {
	'use strict';

	var Question = Parse.Object.extend('Question', {
		defaults: {},
		getType: function() {
			// Is it a linked question?
			if (this.get('primary') && this.get('secondary')) {
				return 'linked';
			} else {
				return this.get('primary').type;
			}
		},
		getScore: function(codeA, codeB) {
			var score, scoreA, scoreB;

			var primary = this.get('primary');
			var indexA = primary.codes.indexOf(codeA);
			scoreA = primary.scores[indexA];

			// Score of LINKED = (absolute value of 1stSCORE - 2ndScore)
			if (this.get('primary') && this.get('secondary')) {

				var secondary = this.get('secondary');
				var indexB = secondary.codes.indexOf(codeB);
				
				scoreB = secondary.scores[indexB];
				score = Math.abs(scoreA + scoreB);

			} else {
				score = scoreA;
			}

			return score;
		}
	});

	return Question;

});