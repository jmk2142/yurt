define([
	'underscore',
	'parse'
], function(_, Parse) {
	'use strict';

	var Response = Parse.Object.extend('Response', {
		defaults: {},
		initialize: function() {
			
		}
	});

	return Response;

});