define([
	'underscore',
	'parse'
], function(_, Parse) {
	'use strict';
	
	var Configuration = Parse.Object.extend('Configuration', {
		defaults: {},
		initialize: function() {
			// console.log('Configuration initialized');
		},
		getCharacter: function(code) {
			return _.find(this.get('characters'), function(character) {
				return character.code === code;
			});
		},
		getProfile: function(code, info) {
			info = info || null;

			var profile = _.find(this.get('profiles'), function(profile) {
				return profile.code === code;
			});

			if (info) {
				if (info === 'personal') {
					return profile[info].hobby + ' ' + profile[info].attitude + ' ' + profile[info].trait;
				} else {
					return profile[info];	
				}
				
			} else {
				return profile;
			}
		}
	});

	return Configuration;

});