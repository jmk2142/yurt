({
	appDir: "../../../",
	baseUrl: "public/lib",
	dir: "../../../../yurt-production",
	modules: [{
		name: "../src/research/main"
	}],
	paths: {
		// Core Paths
		backbone: 'backbone',
		bootstrap: 'bootstrap',
		jquery: 'jquery',
		moment: 'moment',
		parseSDK: 'parse-1.4.2',
		parse: '../src/research/utilities/ParseReady',
		underscore: 'underscore',

		// Helper Paths
		models: '../src/research/models',
		views: '../src/research/views',
		collections: '../src/research/collections',
		utils: '../src/research/utilities',
		tmp: '../templates/research'
	},
	shim: {
		bootstrap: {
			deps: ['jquery'],
			exports: 'jquery'
		},
		parseSDK: {
			exports: 'Parse'
		}
	},
	config: {
		"parse": {
			// For App -> yurt (production)
			appKey: "k7rXCYqMed5AViXrMYbCiMtlXTSN3AqiIX8WL9Km",
			jsKey: "QNuqeYZnFlqLnl9vCJvLFFCNSNNqlZWP7HZpHWvU"
		}
	},
	optimizeCss: 'standard',
	keepBuildDir: true,
	fileExclusionRegExp: /^(sass|index\.html|config|packages\.js|global\.json|\.git|_|\.)/
})