Parse.initialize("k7rXCYqMed5AViXrMYbCiMtlXTSN3AqiIX8WL9Km", "QNuqeYZnFlqLnl9vCJvLFFCNSNNqlZWP7HZpHWvU");

Parse.User.become($('[name="sessionToken"]').attr('content')).then(function(user) {
	if (user) {
		$('button[id^="submit"]').prop('disabled', false);	
	} else {
		console.log('NOTHING');
	}
});

// REFERRAL CODE
var $submit = $('#submitReferral');
var $referral = $('#referral');
var $referralFeedback = $('#referralFeedback');

function saveReferral() {
	Parse.Cloud.run('saveReferral', {
		refSource: $referral.val()
	}).then(function(referral) {
		$referralFeedback.removeClass('in');
		if (feedbackTimer) {
			clearTimeout(feedbackTimer);	
		}
		$referral.attr('placeholder', referral.get('source'));
		$referral.val('');
		$referral.blur();
		$referralFeedback.addClass('in');
		
		var feedbackTimer = _.delay(function() {
			$referralFeedback.removeClass('in');	
		}, 5000);
	});
}

$submit.click(function(event) {
	saveReferral();
});
$referral.keypress(function(event) {
	if (event.which === 13) {
		event.preventDefault();
		saveReferral();
	}
});


// ISSUE CODE
var $submitIssue = $('#submitBugReport');
var $issue = $('#bugReport');
var $issueFeedback = $('#bugReportFeedback');

function saveBugReport() {
	var Issue = Parse.Object.extend('Issue');
	var fullName = 'Anonymous';
	var first = Parse.User.current().get('first');
	var last = Parse.User.current().get('last');

	if (first && last) {
		fullName = first + ' ' + last;
	}

	var issue = new Issue({
		user: Parse.User.current(),
		author: fullName,
		message: $issue.val()
	});

	var acl = new Parse.ACL();
	acl.setRoleReadAccess('admin', true);
	acl.setRoleWriteAccess('admin', true);

	issue.setACL(acl);

	issue.save().then(function(issue) {
		$issueFeedback.removeClass('in');
		if (feedbackTimer) {
			clearTimeout(feedbackTimer);	
		}
		$issue.attr('placeholder', issue.get('message'));
		$issue.val('');
		$issue.blur();
		$issueFeedback.addClass('in');
		
		var feedbackTimer = _.delay(function() {
			$issueFeedback.removeClass('in');	
		}, 5000);
	});
}

$submitIssue.click(function(event) {
	saveBugReport();
});
$issue.keypress(function(event) {
	if (event.which === 13) {
		event.preventDefault();
		saveBugReport();
	}
});


// FEEDBACK CODE
var $submitFeedback = $('#submitFeedback');
var $feedback = $('#feedback');
var $feedbackFeedback = $('#feedbackFeedback');

function saveFeedback() {
	var Feedback = Parse.Object.extend('Feedback');
	var fullName = 'Anonymous';
	var first = Parse.User.current().get('first');
	var last = Parse.User.current().get('last');

	if (first && last) {
		fullName = first + ' ' + last;
	}

	var feedback = new Feedback({
		user: Parse.User.current(),
		author: fullName,
		message: $feedback.val()
	});

	var acl = new Parse.ACL();
	acl.setRoleReadAccess('admin', true);
	acl.setRoleWriteAccess('admin', true);

	feedback.setACL(acl);

	feedback.save().then(function(feedback) {
		$feedbackFeedback.removeClass('in');
		if (feedbackTimer) {
			clearTimeout(feedbackTimer);	
		}
		$feedback.attr('placeholder', feedback.get('message'));
		$feedback.val('');
		$feedback.blur();
		$feedbackFeedback.addClass('in');
		
		var feedbackTimer = _.delay(function() {
			$feedbackFeedback.removeClass('in');	
		}, 5000);
	});
}

$submitFeedback.click(function(event) {
	saveFeedback();
});
$feedback.keypress(function(event) {
	if (event.which === 13) {
		event.preventDefault();
		saveFeedback();
	}
});
