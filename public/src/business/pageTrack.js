// http://stackoverflow.com/a/4825695/972393
function createCookie(name, value, days) {
	var expires;
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toGMTString();
	}
	else {
		expires = "";
	}
	document.cookie = name + "=" + value + expires + "; path=/";
}
// http://stackoverflow.com/a/4825695/972393
function getCookie(c_name) {
	if (document.cookie.length > 0) {
		c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) {
					c_end = document.cookie.length;
			}
			return unescape(document.cookie.substring(c_start, c_end));
		}
	}
	return "";
}

function getVideoStatus() {
	var started = getCookie('started');
	var watched = getCookie('watched');

	if (watched === 'true') {
		return 'watched';
	} else if (started === 'true') {
		return 'started';
	} else {
		return 'unstarted';	
	}
	
}

var page = $('meta[name="page"]').attr('content');

var data = {
	page: page,
	version: getCookie('fPage'),
	video: getVideoStatus()
};

Parse.Analytics.track('pageView', data);
mixpanel.track('pageView', data);

// YOUTUBE
// http://stackoverflow.com/questions/7853904/how-to-detect-when-a-youtube-video-finishes-playing
var player;

function onYouTubePlayerAPIReady() {
	player = new YT.Player('yurtVideo', {
		videoId: '06vu4oQeWaM',
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
}

function onPlayerReady(event) {
	mixpanel.track('video', {
		status: 'unstarted'
	});
}

// Event that fires when video state changes
// -1: unstarted, 0: ended, 1: playing, 2: paused, 3: buffering, 5: video cued
function onPlayerStateChange(event) {

	if (getVideoStatus() !== 'watched') {
		if (event.data === 0) {
			// Create video
			createCookie('watched', 'true');
			mixpanel.track('video', {
				status: 'watched'
			});
		} else if (event.data === 1) {
			// Create video started
			createCookie('started', 'true');
			mixpanel.track('video', {
				status: 'started'
			});
		}
	}

}

mixpanel.track_links('a[href="consent"]', 'click link', {
	"link": "consent",
	"version": getCookie('fPage'),
	"referrer": document.referrer
});
