Parse.initialize("k7rXCYqMed5AViXrMYbCiMtlXTSN3AqiIX8WL9Km", "QNuqeYZnFlqLnl9vCJvLFFCNSNNqlZWP7HZpHWvU");
Parse.User.logOut();

window.yurt = {
	saveLocal: function(item, obj) {
		var yurt = JSON.parse(window.localStorage.getItem('yurt')) || {};
				yurt[item] = obj;

		var jsonStr = JSON.stringify(yurt);
		window.localStorage.setItem('yurt', jsonStr);
	},
	getLocal: function(item) {
		var json = JSON.parse(window.localStorage.getItem('yurt'));
		if (item) {
			return json[item];
		} else {
			return json;
		}
	},
	removeLocal: function(item) {
		var yurt = JSON.parse(window.localStorage.getItem('yurt')) || {};

		delete yurt[item];

		var jsonStr = JSON.stringify(yurt);
		window.localStorage.setItem('yurt', jsonStr);
	}
};

// Click Handler
$('#getUsername').click(function(event) {
	// Spin
	$('.fa-refresh').addClass('fa-spin');

	$('#username').val('');

	Parse.Cloud.run('getUsername').then(function(username) {
		$('#username').val(username);

		// Unspin
		$('.fa-refresh').attr('class', 'fa fa-refresh');
	});
});

function validateInput($input) {
	var $target = $input;
	var value = $target.val();
	var inputID = $target.attr('id');
	var valid = false;

	// Multiple Condition Switch
	switch (inputID) {
		case 'password':
			if (value.length >= 8) {
				valid = true;
			}
			break;
		case 'confirm':
			if (value === $('#password').val()) {
				valid = true;
			}
			break;
		case 'email':
			if ($target.is(':valid')) {
				valid = true;
			}
			break;
		case 'consent':
			if ($target.is(':checked')) {
				valid = true;
			}
			break;
		default:
			if (value.length > 0) {
				valid = true;
			}
			break;
	}

	return valid;
}
function addFeedback($input, valid) {
	var $target = $input;
	// Reset feedback classes
	$target.closest('.form-group').attr('class', 'form-group');	

	if ($target.attr('id') !== 'consent') {
		if (valid) {
			$target.closest('.form-group').addClass('has-success');
			$target.siblings('.help-block').addClass('hidden');
		} else {
			$target.closest('.form-group').addClass('has-error');
			$target.siblings('.help-block').removeClass('hidden');
		}
	} else {
		if (valid) {
			$target.siblings('.help-block').addClass('hidden');
			$target.siblings('.text-danger').addClass('hidden');
		} else {
			$target.siblings('.help-block').removeClass('hidden');
			$target.siblings('.text-danger').removeClass('hidden');
		}
	}
}

// Validation
$('input').blur(function(event) {
	var $target = $(event.currentTarget);
	var valid = validateInput($target);

	addFeedback($target, valid);
});

// Override submit
$('form[action="register"]').submit(function(event) {

	// Analytics - Register Action
	var page = $('meta[name="page"]').attr('content');
	var data = {
		action: 'registered',
		version: getCookie('fPage'),
		video: getVideoStatus()
	};
	Parse.Analytics.track('registration', data);
	mixpanel.track('registration', data);


	// Form Validation
	var validForm = false;
	var validPassword = validConfirm = validFirst = validLast = validEmail = validConsent = false;

	validPassword = validateInput($('#password'));
	validConfirm = validateInput($('#confirm'));
	validFirst = validateInput($('#first'));
	validLast = validateInput($('#last'));
	validEmail = validateInput($('#email'));
	validConsent = validateInput($('#consent'));

	validForm = (validPassword && validConfirm && validFirst && validLast && validEmail && validConsent);

	// Final validation here...

	if (validForm) {
		yurt.saveLocal($('#username').val(), {
			first: $('#first').val(),
			last: $('#last').val(),
			username: $('#username').val()
		});
		return;
	} else {
		event.preventDefault();

		addFeedback($('#password'), validPassword);
		addFeedback($('#confirm'), validConfirm);
		addFeedback($('#first'), validFirst);
		addFeedback($('#last'), validLast);
		addFeedback($('#email'), validEmail);
	}
});
